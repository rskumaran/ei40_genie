package com.entappia.ei40genie.utils;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;

@Component
public class TagScheduleAsyncTask {

	public String TAG_NAME = "TagScheduleTask";



	@Autowired
	private TagBackgroundTasks tagBackgroundTasks;
	

	public void sendTags() {
		try {

			if (AppConstants.print_log)
				System.out.println(TAG_NAME + " Task Start Time:" + Utils.getFormatedDate1(new Date()));

				// ToDo:Shiva Check this. consider all cases. deleting old data from tables
				tagBackgroundTasks.sendTags();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (AppConstants.print_log)
			System.out.println(TAG_NAME + " Task End Time:" + Utils.getFormatedDate1(new Date()));
	}


}
