package com.entappia.ei40genie.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.constants.AppConstants;

@Component
public class TagSchedulerTask {
	private static ThreadPoolTaskScheduler helloApiTaskScheduler;
	private static boolean isRunning;
	public String TAG_NAME = "TagSchedulerTask";

	Preference preference = new Preference();
	
	@Autowired
	private TagScheduleAsyncTask tagScheduleAsyncTask;
	
	public void createSchedule() {

		if (helloApiTaskScheduler == null) {
			helloApiTaskScheduler = new ThreadPoolTaskScheduler();
			helloApiTaskScheduler.setPoolSize(5);
			helloApiTaskScheduler.initialize();
		}

		startSchedule();
	}

	public void startSchedule() {

		String cronTime = "0 0/1 * * * *";
		if (AppConstants.print_log)
			System.out.println("HelloApiTask cronTime: " + cronTime);

		if (!isRunning) {
			if (helloApiTaskScheduler != null)
				helloApiTaskScheduler.schedule(new RunnableTask(), new CronTrigger(cronTime));

			isRunning = true;
		}
	}

	public void stopSchedule() {
		if (helloApiTaskScheduler != null)
		{
			helloApiTaskScheduler.getScheduledThreadPoolExecutor().shutdown();
			helloApiTaskScheduler.shutdown();
		}

		helloApiTaskScheduler = null;
		isRunning = false;
	}

	public void changeScheduleTime() {
		stopSchedule();
		createSchedule();
		startSchedule();
	}
	
	class RunnableTask implements Runnable {

		public RunnableTask() {
		}

		@SuppressWarnings("unused")
		@Override
		public void run() {

			tagScheduleAsyncTask.sendTags();
		}
	};
	
}
