package com.entappia.ei40genie.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entappia.ei40genie.models.HelloApiData;
import com.entappia.ei40genie.repository.CampusDetailsRepository;
import com.entappia.ei40genie.repository.ConfigurationRepository;
import com.entappia.ei40genie.repository.DoorsRepository;
import com.entappia.ei40genie.repository.FirmwareSettingsRepository;
import com.entappia.ei40genie.repository.OutLineRepository;
import com.entappia.ei40genie.repository.PartitionsRepository;
import com.entappia.ei40genie.repository.PathWayRepository;
import com.entappia.ei40genie.repository.TagsRepository;
import com.entappia.ei40genie.repository.WallsRepository;
import com.entappia.ei40genie.repository.ZoneClassificationRepository;
import com.entappia.ei40genie.repository.ZoneRepository;
import com.entappia.ei40genie.constants.AppConstants.ZoneType;
import com.entappia.ei40genie.dbmodels.CampusDetails;
import com.entappia.ei40genie.dbmodels.Configuration;
import com.entappia.ei40genie.dbmodels.Doors;
import com.entappia.ei40genie.dbmodels.FirmwareSettings;
import com.entappia.ei40genie.dbmodels.OutlinesDetails;
import com.entappia.ei40genie.dbmodels.Partitions;
import com.entappia.ei40genie.dbmodels.PathWay;
import com.entappia.ei40genie.dbmodels.Tags;
import com.entappia.ei40genie.dbmodels.Walls;
import com.entappia.ei40genie.dbmodels.ZoneClassification;
import com.entappia.ei40genie.dbmodels.ZoneDetails;

@Service
public class HelloApiDataConstruct {

	HelloApiData helloApiData = new HelloApiData();

	@Autowired
	private ConfigurationRepository configurationRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private ZoneRepository zoneRepository;

	@Autowired
	private ZoneClassificationRepository zoneClassificationRepository;

	@Autowired
	private FirmwareSettingsRepository firmwareSettingsRepository;

	@Autowired
	private DoorsRepository doorsRepository;

	@Autowired
	private WallsRepository wallsRepository;

	@Autowired
	private PathWayRepository pathwayRepository;

	@Autowired
	private PartitionsRepository partitionRepository;

	@Autowired
	private OutLineRepository outLineDetailsRepository;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	private  Date tagColorLastSentDate = null;
	private  Date tagDetailsLastSentDate = null;
	private  Date removedZoneDetailsLastSentDate = null;
	private  Date addedZoneDetailsLastSentDate = null;
	private  Date removedMapDetailsLastSentDate = null;
	private  Date applicationVersionsLastSentDate = null;
	private  Date addedMapDetailsLastSentDate = null;
	private  Date outlineDetailsLastSentDate = null;
	private  Date partitionsLastSentDate = null;
	private  Date pathwayLastSentDate = null;
	private  Date doorsLastSentDate = null;
	private  Date wallsLastSentDate = null;

	public HelloApiData getHelloApiData() {
		clearHelloApiData();
		generateHelloApiData();
		return helloApiData;
	}

	private void clearHelloApiData() {
		helloApiData.clear();
	}

	public void generateHelloApiData() {
		frameTagColorDetails();
		frameTagDetails();
		frameRemoveZoneDetails();
		frameAddZoneDetails();
		frameremoveMapDetails();
		frameApplicationVersions();
		frameMapDetails();
		frameOutlineDetailsList();
		framePartitionsList();
		framePathwayList();
		frameDoorsList();
		frameWallsList();

	}
	private void frameWallsList() {
		
		Date today = new Date();
		List<Walls> wallsList  = wallsRepository.findAllActiveWalls(); 
		List<HashMap<String, Object>> allWallsMap = new ArrayList<>();

		boolean wallsModified = false;
		
		for(int i=0;i < wallsList.size();i++)
		{

			Walls walls = wallsList.get(i);


			if(walls.isActive() == true) {
				if(wallsLastSentDate == null || (walls.getModifiedDate() != null && walls.getModifiedDate().after(wallsLastSentDate))){
					wallsModified = true;
					HashMap<String, Object> wallsMap = new HashMap<>();

					wallsMap.put("id", walls.getId());
					wallsMap.put("name", walls.getName());
					wallsMap.put("colorCode", walls.getColorCode());
					wallsMap.put("coordinatesList", walls.getCoordinates());
					wallsMap.put("wallType", walls.getWallType());
					wallsMap.put("outlinesId", walls.getOutlinesDetails().getOutlinesId());
					wallsMap.put("geometryType", walls.getGeometryType());

					allWallsMap.add(wallsMap);
				}
			}
		}
		if(true == wallsModified){
			wallsLastSentDate = today;
		}
		
		if(allWallsMap != null && allWallsMap.size() > 0) {
			helloApiData.setWalls(allWallsMap);
		}
		

	}

	private void frameDoorsList() {
		Date today = new Date();
		List<Doors> doorsList = new ArrayList<>();
		doorsList = doorsRepository.findAllActiveDoors();
		boolean doorsModified = false;

		List<HashMap<String, Object>> allDoorsMap = new ArrayList<>();
		
		 

		for(int i=0;i < doorsList.size();i++)
		{

			Doors doors = doorsList.get(i);


			if(doors.isActive() == true) {
				if(doorsLastSentDate == null || (doors.getModifiedDate() != null && doors.getModifiedDate().after(doorsLastSentDate))){
					doorsModified = true;
				HashMap<String, Object> doorsMap = new HashMap<>();

				doorsMap.put("id", doors.getId());
				doorsMap.put("name", doors.getName());
				doorsMap.put("colorCode", doors.getColorCode());
				doorsMap.put("coordinatesList", doors.getCoordinates());
				doorsMap.put("wallType", doors.getWallType());
				doorsMap.put("outlinesId", doors.getOutlinesDetails().getOutlinesId());

				allDoorsMap.add(doorsMap );
				}
			}
		}
		
		if(true == doorsModified){
			doorsLastSentDate = today;
		}
		if(allDoorsMap != null && allDoorsMap.size() > 0) {
			helloApiData.setDoors(allDoorsMap);
		}
		

	}

	private void framePathwayList() {
		Date today = new Date();
		List<PathWay> pathWayList = new ArrayList<>();
		pathWayList = pathwayRepository.findAll();
		boolean pathwayModified = false;
		
		List<HashMap<String, Object>> allPathWaysMap = new ArrayList<>();

		for(int i=0;i < pathWayList.size();i++)
		{

			PathWay pathWay = pathWayList.get(i);


			if(pathWay.isActive() == true) {
				if(pathwayLastSentDate == null || (pathWay.getModifiedDate() != null && pathWay.getModifiedDate().after(pathwayLastSentDate))){
					pathwayModified = true;
					
				HashMap<String, Object> pathWayMap = new HashMap<>();

				pathWayMap.put("id", pathWay.getId());
				pathWayMap.put("name", pathWay.getName());
				pathWayMap.put("colorCode", pathWay.getColorCode());
				pathWayMap.put("coordinatesList", pathWay.getCoordinates());
				pathWayMap.put("geometryType", pathWay.getGeometryType());
				pathWayMap.put("pathwayPattern", pathWay.getPathwayPattern());
				pathWayMap.put("pathwayType", pathWay.getPathwayType());

				allPathWaysMap.add(pathWayMap );
				}
			}
		}
		
		if(true == pathwayModified){
			pathwayLastSentDate = today;
		}
		
		if(allPathWaysMap != null && allPathWaysMap.size() > 0) {
			helloApiData.setPathways(allPathWaysMap);
		}
		

	}

	private void framePartitionsList() {
		Date today = new Date();
		List<Partitions> partitionsList = new ArrayList<>();
		partitionsList = partitionRepository.findAllActivePartitions();
		boolean partitionModified = false;
		
		List<HashMap<String, Object>> allPartitionsMap = new ArrayList<>();

		for(int i=0;i < partitionsList.size();i++)
		{

			Partitions partitions = partitionsList.get(i);


			if(partitions.isActive() == true) {
				if(partitionsLastSentDate == null || (partitions.getModifiedDate() != null && partitions.getModifiedDate().after(partitionsLastSentDate))){
					partitionModified = true;
					
				HashMap<String, Object> partitionsMap = new HashMap<>();

				partitionsMap.put("id", partitions.getId());
				partitionsMap.put("name", partitions.getName());
				partitionsMap.put("colorCode", partitions.getColorCode());
				partitionsMap.put("coordinatesList", partitions.getCoordinates());
				partitionsMap.put("geometryType", partitions.getGeometryType());
				partitionsMap.put("wallType", partitions.getWallType());
				partitionsMap.put("outlinesId", partitions.getOutlinesDetails().getOutlinesId());

				allPartitionsMap.add(partitionsMap );
				}
			}
		}
		if(true == partitionModified){
			partitionsLastSentDate = today;
		}
		if(allPartitionsMap != null && allPartitionsMap.size() > 0) {
			helloApiData.setPartitions(allPartitionsMap);
		}
		

	}

	private void frameOutlineDetailsList() {
		Date today = new Date();
		List<OutlinesDetails> outlinesDetailsList = new ArrayList<>();
		outlinesDetailsList = outLineDetailsRepository.findAll();
		boolean outlineModified = false;

		List<HashMap<String, Object>> allOutlinesDetailsMap = new ArrayList<>();

		for(int i=0;i < outlinesDetailsList.size();i++)
		{

			OutlinesDetails outlinesDetails = outlinesDetailsList.get(i);


			if(outlinesDetails.isActive() == true) {
				if(outlineDetailsLastSentDate == null || (outlinesDetails.getModifiedDate() != null && outlinesDetails.getModifiedDate().after(outlineDetailsLastSentDate))){
					outlineModified = true;
				HashMap<String, Object> outlinesDetailsMap = new HashMap<>();

				outlinesDetailsMap.put("name", outlinesDetails.getName());
				outlinesDetailsMap.put("outlinesId", outlinesDetails.getOutlinesId());
				outlinesDetailsMap.put("bgColor", outlinesDetails.getBgColor());
				outlinesDetailsMap.put("coordinateList", outlinesDetails.getCoordinateList());
				outlinesDetailsMap.put("geometryType", outlinesDetails.getGeometryType());
				outlinesDetailsMap.put("outlinesArea", outlinesDetails.getOutlinesArea());
				outlinesDetailsMap.put("placeType", outlinesDetails.getPlaceType());
				outlinesDetailsMap.put("wallType", outlinesDetails.getWallType());

				allOutlinesDetailsMap.add(outlinesDetailsMap );
				}
			}
		}
		if(true == outlineModified){
			outlineDetailsLastSentDate = today;
		}
		if(allOutlinesDetailsMap != null && allOutlinesDetailsMap.size() > 0) {
			helloApiData.setOutlines(allOutlinesDetailsMap);
		}
		

	}

	private void frameMapDetails() {
		Date today = new Date();
		List<CampusDetails> campusDetailsList = new ArrayList<>();
		campusDetailsList = campusDetailsRepository.findAll();
		boolean mapModified = false;

		for(int i=0;i < campusDetailsList.size();i++)
		{

			CampusDetails campusDetails = campusDetailsList.get(i);

			if(campusDetails.isActive() == true) {
				if(addedMapDetailsLastSentDate == null || (campusDetails.getModifiedDate() != null && campusDetails.getModifiedDate().after(addedMapDetailsLastSentDate))){
					mapModified = true;
					
					helloApiData.setMapDetails("name", campusDetails.getName());
					helloApiData.setMapDetails("colorCode", campusDetails.getColorCode());
					helloApiData.setMapDetails("height", campusDetails.getHeight());
					helloApiData.setMapDetails("width", campusDetails.getWidth());
					helloApiData.setMapDetails("ppmX", campusDetails.getMetersPerPixelX());
					helloApiData.setMapDetails("ppmY", campusDetails.getMetersPerPixelY());
					helloApiData.setMapDetails("originX", campusDetails.getOriginX());
					helloApiData.setMapDetails("originY", campusDetails.getOriginY());
				}

			}
		}
		if(true == mapModified){
			addedMapDetailsLastSentDate = today;
		}

	}

	private void frameApplicationVersions() {
		Date today = new Date();
		List<FirmwareSettings> firmwareSettingsList = new ArrayList<>();
		firmwareSettingsList = firmwareSettingsRepository.findAll();
		boolean versionModified = false;
		for(int i=0;i < firmwareSettingsList.size();i++)
		{
			FirmwareSettings firmwareSettings = firmwareSettingsList.get(i);
			//if(applicationVersionsLastSentDate == null || (firmwareSettings.getModifiedDate() == null && firmwareSettings.getModifiedDate().after(applicationVersionsLastSentDate))){
				versionModified = true;
			helloApiData.setApplicationVersion(firmwareSettings.getApplication(), firmwareSettings.getVersion());
			//}
		}
		if(true == versionModified){
			applicationVersionsLastSentDate = today;
		}

	}

	private void frameremoveMapDetails() {
		Date today = new Date();
		HashMap<String, List<String>> removeMapDetails =  new HashMap<>();
		boolean mapModified = false;
		
		List<Doors> doorsList = new ArrayList<>();
		doorsList = doorsRepository.findAll();

		List<Walls> wallsList = new ArrayList<>();
		wallsList = wallsRepository.findAll();

		List<PathWay> pathWayList = new ArrayList<>();
		pathWayList = pathwayRepository.findAll();

		List<Partitions> partitionsList = new ArrayList<>();
		partitionsList = partitionRepository.findAll();

		List<OutlinesDetails> outlineDetailsList = new ArrayList<>();
		outlineDetailsList = outLineDetailsRepository.findAll();


		List<String> doorsName = new ArrayList<>();
		for(int i=0;i < doorsList.size();i++)
		{
			Doors doors = doorsList.get(i);

			if(doors.isActive() == false) {
				if(removedMapDetailsLastSentDate == null || (doors.getModifiedDate() != null && doors.getModifiedDate().after(removedMapDetailsLastSentDate))){
					mapModified = true;
				doorsName.add(doors.getName());
				}
			}
		}
		if(doorsName != null && doorsName.size() > 0) {
			removeMapDetails.put("door", doorsName);
		}
			

		List<String> wallName = new ArrayList<>();
		for(int i=0;i < wallsList.size();i++)
		{
			Walls walls = wallsList.get(i);

			if(walls.isActive() == false) {
				if(removedMapDetailsLastSentDate == null || (walls.getModifiedDate() != null && walls.getModifiedDate().after(removedMapDetailsLastSentDate))){
					mapModified = true;
				wallName.add(walls.getName());
				}
			}
		}

		if(wallName != null && wallName.size() > 0) {
			removeMapDetails.put("walls", wallName);
		}
		

		List<String> pathwayName = new ArrayList<>();
		for(int i=0;i < pathWayList.size();i++)
		{
			PathWay pathway = pathWayList.get(i);

			if(pathway.isActive() == false) {
				if(removedMapDetailsLastSentDate == null || (pathway.getModifiedDate() != null && pathway.getModifiedDate().after(removedMapDetailsLastSentDate))){
					mapModified = true;
				pathwayName.add(pathway.getName());
				}
			}
		}

		if(pathwayName != null && pathwayName.size() > 0) {
			removeMapDetails.put("pathway", pathwayName);
		}
		
		


		List<String> partitionName = new ArrayList<>();
		for(int i=0;i < partitionsList.size();i++)
		{
			Partitions partitions = partitionsList.get(i);

			if(partitions.isActive() == false) {
				if(removedMapDetailsLastSentDate == null || (partitions.getModifiedDate() != null && partitions.getModifiedDate().after(removedMapDetailsLastSentDate))){
					mapModified = true;
				partitionName.add(partitions.getName());
				}
			}
		}

		if(partitionName != null && partitionName.size() > 0) {
			removeMapDetails.put("partition", partitionName);
		}
		


		List<String> outlineDetailsName = new ArrayList<>();
		for(int i=0;i < outlineDetailsList.size();i++)
		{
			OutlinesDetails outlinesDetails = outlineDetailsList.get(i);

			if(outlinesDetails.isActive() == false) {
				if(removedMapDetailsLastSentDate == null || (outlinesDetails.getModifiedDate() != null && outlinesDetails.getModifiedDate().after(removedMapDetailsLastSentDate))){
					mapModified = true;
				outlineDetailsName.add(outlinesDetails.getName());
				}
			}
		}

		if(outlineDetailsName != null && outlineDetailsName.size() > 0) {
			removeMapDetails.put("outline", outlineDetailsName);
		}
		
		

		if(true == mapModified){
			removedMapDetailsLastSentDate = today;
		}

		if(removeMapDetails != null && removeMapDetails.size() > 0) {
			helloApiData.setRemoveMapDetails(removeMapDetails);
		}

		

	}

	private void frameAddZoneDetails() {

		Date today = new Date();
		HashMap<String, Object> addZoneDetails =  new HashMap<>();

		List<ZoneClassification> zoneClassificationList = new ArrayList<>();
		zoneClassificationList = zoneClassificationRepository.findAll();
		boolean zoneModified = false;
		
		HashMap<String, Object> zoneClassName = new HashMap<>();
		for(int i=0;i < zoneClassificationList.size();i++)
		{
			ZoneClassification zoneClassification = zoneClassificationList.get(i);

			if(zoneClassification.isActive() == true) {
				if(addedZoneDetailsLastSentDate == null || (zoneClassification.getModifiedDate() != null && zoneClassification.getModifiedDate().after(addedZoneDetailsLastSentDate))){
					zoneModified = true;
				zoneClassName.put(zoneClassification.getClassName(), zoneClassification.getColor());
				}
			}
		}

		if(zoneClassName != null && zoneClassName.size() > 0) {
			addZoneDetails.put("classifications", zoneClassName);
		}
		


		List<ZoneDetails> zoneDetailsList = new ArrayList<>();
		zoneDetailsList = zoneRepository.findAll();

		List<HashMap<String, Object>> allZoneDetailsMap = new ArrayList<HashMap<String, Object>>();

		for(int i=0;i < zoneDetailsList.size();i++)
		{

			ZoneDetails zoneDetails = zoneDetailsList.get(i);

			if(zoneDetails.isActive() == true) {
				if(addedZoneDetailsLastSentDate == null || (zoneDetails.getModifiedDate() != null && zoneDetails.getModifiedDate().after(addedZoneDetailsLastSentDate))){
					zoneModified = true;
				HashMap<String, Object> ZoneDetailsMap = new HashMap<>();



				ZoneDetailsMap.put("id", String.valueOf(zoneDetails.getId()));
				ZoneDetailsMap.put("name", zoneDetails.getName());
				ZoneDetailsMap.put("accessControl", zoneDetails.getAccessControl());
				ZoneDetailsMap.put("area", zoneDetails.getArea());
				ZoneDetailsMap.put("capacity", zoneDetails.getCapacity());
				ZoneDetailsMap.put("coordinateList", zoneDetails.getCoordinateList()); 
				ZoneDetailsMap.put("userType", zoneDetails.getType());
				ZoneDetailsMap.put("zoneType", zoneDetails.getZoneType()==ZoneType.Trackingzone?"Tracking Zone":"Dead Zone");
				ZoneDetailsMap.put("classificationName", zoneDetails.getZoneClassification().getClassName());
				ZoneDetailsMap.put("geometryType", zoneDetails.getGeometryType());
				ZoneDetailsMap.put("descriptions", zoneDetails.getDescription()); 

				allZoneDetailsMap.add(ZoneDetailsMap );
				}
			}
		}
		if(allZoneDetailsMap != null && allZoneDetailsMap.size() > 0) {
			addZoneDetails.put("zones", allZoneDetailsMap);
		}
		

		if(true == zoneModified){
			addedZoneDetailsLastSentDate = today;
		}
		if(addZoneDetails != null && addZoneDetails.size() > 0) {
			helloApiData.setAddZoneDetails(addZoneDetails);
		}
		


	}

	private void frameRemoveZoneDetails() {

		Date today = new Date();
		HashMap<String, List<String>> removeZoneDetails =  new HashMap<>();

		List<ZoneClassification> zoneClassificationList = new ArrayList<>();
		zoneClassificationList = zoneClassificationRepository.findAll();

		List<ZoneDetails> zoneDetailsList = new ArrayList<>();
		zoneDetailsList = zoneRepository.findAll();
		
		boolean zoneModified = false;


		List<String> zoneClassName = new ArrayList<>();
		for(int i=0;i < zoneClassificationList.size();i++)
		{
			ZoneClassification zoneClassification = zoneClassificationList.get(i);

			if(zoneClassification.isActive() == false) {
				if(removedZoneDetailsLastSentDate == null || (zoneClassification.getModifiedDate() != null && zoneClassification.getModifiedDate().after(removedZoneDetailsLastSentDate))){
					zoneModified = true;
				zoneClassName.add(zoneClassification.getClassName());
				}
			}
		}

		if(zoneClassName != null && zoneClassName.size() > 0) {
			removeZoneDetails.put("classifications", zoneClassName);
		}
		

		List<String> zoneId = new ArrayList<>();
		for(int i=0;i < zoneDetailsList.size();i++)
		{
			ZoneDetails zoneDetails = zoneDetailsList.get(i);

			if(zoneDetails.isActive() == false) {
				if(removedZoneDetailsLastSentDate == null || (zoneDetails.getModifiedDate() != null && zoneDetails.getModifiedDate().after(removedZoneDetailsLastSentDate))){
					zoneModified = true;
				zoneId.add(String.valueOf(zoneDetails.getId()));
				}
			}
		}
		if(zoneId != null && zoneId.size() > 0) {
			removeZoneDetails.put("zones",zoneId);
		}
		

		if(true == zoneModified){
			removedZoneDetailsLastSentDate = today;
		}
		if(removeZoneDetails != null && removeZoneDetails.size() > 0) {
			helloApiData.setRemoveZoneDetails(removeZoneDetails);
		}
		


	}

	private void frameTagColorDetails() {
		Date today = new Date();
		List<Configuration> configurationDetails = new ArrayList<>();
		configurationDetails = configurationRepository.findAll();

		boolean tagColorModified = false;

		for(int i=0;i < configurationDetails.size();i++)
		{
			Configuration configuration = configurationDetails.get(i);
			if(configuration.isActive() == true) {
				if(tagColorLastSentDate == null || (configuration.getModifiedDate() != null && configuration.getModifiedDate().after(tagColorLastSentDate))){
					tagColorModified = true;
					helloApiData.setTagColorDetails("Warehouse",configuration.getWareHouseTagColor());
					helloApiData.setTagColorDetails("Visitor",configuration.getVisitorTagColor());
					helloApiData.setTagColorDetails( "RMA", configuration.getRmaTagColor());
					helloApiData.setTagColorDetails("Staff",configuration.getStaffTagColor());
					helloApiData.setTagColorDetails("Material", configuration.getMaterialTagColor());
					helloApiData.setTagColorDetails("Vendor", configuration.getVendorTagColor());
					helloApiData.setTagColorDetails("LSLM", configuration.getLSLMTagColor());
					helloApiData.setTagColorDetails("Vehicle", configuration.getVehicleTagColor());
					helloApiData.setTagColorDetails("Contractor",configuration.getContractorTagColor());
					helloApiData.setTagColorDetails("Parts", configuration.getPartTagColor());
					helloApiData.setTagColorDetails("Gage", configuration.getGageTagColor());
					helloApiData.setTagColorDetails( "Fixture", configuration.getFixtureTagColor());
					helloApiData.setTagColorDetails("Supplies",configuration.getSuppliesTagColor());
					helloApiData.setTagColorDetails("Inventory", configuration.getInventoryTagColor());
					helloApiData.setTagColorDetails("Tool", configuration.getToolTagColor());
				}
			}
		}
		if(true == tagColorModified){
			tagColorLastSentDate = today;
		}
	}

	private void frameTagDetails() {
		Date today = new Date();
		List<Tags> tags = new ArrayList<>();
		tags = tagsRepository.findAll();
		boolean tagDetailsModified = false;

		for(int i=0;i < tags.size();i++)
		{
			Tags tag = tags.get(i);

			if(tagDetailsLastSentDate == null || (tag.getModifiedDate() != null && tag.getModifiedDate().after(tagDetailsLastSentDate))){
				tagDetailsModified = true;
			HashMap<String, Object> tagDetails = new HashMap<>();
			tagDetails.put("macAddress", tag.getMacId());
			tagDetails.put("assignmentId", tag.getAssignmentId());
			tagDetails.put("assignmentDate", tag.getAssignmentDate());
			tagDetails.put("status", tag.getStatus());
			tagDetails.put("tagId", tag.getTagId());
			tagDetails.put("type", tag.getType());
			tagDetails.put("subType", tag.getSubType());
			helloApiData.setTagDetails(tagDetails);
			}
		}
		if(true == tagDetailsModified){
			tagDetailsLastSentDate = today;
		}
	
	}
	public void FrameHelloApiData() {
		clearHelloApiData();
		generateHelloApiData();
	}

	public HashMap<String, Object> getTagColorDetails() {
		return helloApiData.getTagColorDetails();
	}
	public List<HashMap<String, Object>> getTagDetails() {
		return helloApiData.getTagDetails();
	}

	public HashMap<String, List<String>> getRemoveZoneDetails() {
		return helloApiData.getRemoveZoneDetails();
	}

	public Object getAddZoneDetails() {
		return helloApiData.getAddZoneDetails();
	}

	public HashMap<String, Object> getApplicationVersion() {
		return helloApiData.getApplicationVersion();
	}

	public HashMap<String, List<String>> getRemoveMapDetails() {
		return helloApiData.getRemoveMapDetails();
	}
	public HashMap<String, Object> getMapDetails() {
		return helloApiData.getMapDetails();
	}


	public Object getOutlines() {
		return helloApiData.getOutlines();
	}

	public Object getPartitions() {
		return helloApiData.getPartitions();
	}

	public Object getDoors() {
		return helloApiData.getDoors();
	}

	public Object getWalls() {
		return helloApiData.getWalls();
	}

	public Object getPathways() {
		return helloApiData.getPathways();
	}

}
