package com.entappia.ei40genie.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.dbmodels.TagPosition;
import com.entappia.ei40genie.dbmodels.Tags;
import com.entappia.ei40genie.entappia.EntappiaApiService;
import com.entappia.ei40genie.repository.TagPositionRepository;
import com.entappia.ei40genie.repository.TagsRepository;
import com.fasterxml.jackson.annotation.JsonFormat;

@Component
public class TagBackgroundTasks {

	@Autowired
	Logger logger;
	
	@Autowired
	private EntappiaApiService entappiaApiService;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private TagPositionRepository tagPositionRepository;
	
	@Autowired
	private SendTagPositionAsyncTask sendTagPositionAsyncTask;

	@Autowired
	private EntappiaGenieApplication entappiaGenieApplication;

	Preference preference = new Preference();

	// ToDo:shiva
	public void sendTags() {
		try {

			if (!EntappiaGenieApplication.isServerConnected) {
				entappiaGenieApplication.getSessionData();
				return;
			}

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {

					try {
						List<HashMap<String, Object>> tagList = new ArrayList<>();

						try {

							List<Tags> tagListFromDB = tagsRepository.findAll();

							List<TagPosition> tagPositionListFromDB = tagPositionRepository.findAll();

							HashMap<String, TagPosition> tagPositionMap = new HashMap<>();

							if (tagPositionListFromDB != null) {
								tagPositionListFromDB.forEach(tagPosition -> {
									tagPositionMap.put(tagPosition.getMacId(), tagPosition);
								});
							}

							for (int i = 0; i < tagListFromDB.size(); i++) {
								Tags tag = tagListFromDB.get(i);

								HashMap<String, Object> hashMap = new HashMap<>();

								hashMap.put("macAddress", tag.getMacId());
								hashMap.put("assignmentId", tag.getAssignmentId());
								if(tag.getAssignmentDate() != null) {
								hashMap.put("assignmentDate", tag.getAssignmentDate().toString());
								}
								hashMap.put("status", tag.getStatus());
								hashMap.put("tagId", tag.getTagId());
								hashMap.put("type", tag.getType());
								hashMap.put("subType", tag.getSubType());

								TagPosition tagPosition = tagPositionMap.get(tag.getMacId());

								HashMap<String, Object> positionMap = new HashMap<>();
								if (tagPosition != null) {
									positionMap.put("x", tagPosition.getX());
									positionMap.put("y", tagPosition.getY());
									positionMap.put("z", tagPosition.getZ());
									positionMap.put("zoneName", tagPosition.getZoneName());
									positionMap.put("lastSeen", tagPosition.getLastSeen());
									hashMap.put("positions", tagPosition);
								}

								/*
								 * hashMap.put("groupId", tag.getGroupId()); hashMap.put("major",
								 * String.valueOf(tag.getMajor())); hashMap.put("minor",
								 * String.valueOf(tag.getMinor()));
								 * 
								 * hashMap.put("manufacturer", tag.getManufacturer()); hashMap.put("restart",
								 * String.valueOf(tag.isRestart())); hashMap.put("isRenamed",
								 * String.valueOf(tag.isRenamed())); hashMap.put("restartDate",
								 * tag.getRestartDate().toString());
								 * 
								 * hashMap.put("assignmentId", tag.getAssignmentId());
								 * 
								 * hashMap.put("createdDate", tag.getCreatedDate().toString());
								 * hashMap.put("modifiedDate", tag.getModifiedDate().toString());
								 */

								tagList.add(hashMap);
							}

							if (tagList != null && tagList.size() > 0) {

								entappiaApiService.sendTagsDetails(tagList);
							}
						} catch (Exception e) {
							addLogs("Error", "Genie", "Get QuuppaTag-Background", e.getMessage());
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} catch (Exception e) {
						addLogs("Error", "Genie", "Get QuuppaTag-Background", e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			executorService.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void sendTagPositions() {
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		executorService.execute(new Runnable() {

			public void run() {

				try {
					
					try {
						List<TagPosition> tagPositionListFromDB  = tagPositionRepository.findAll();

						List<HashMap<String, Object>> tagPositionMap = new ArrayList();
						for (int i = 0; i < tagPositionListFromDB.size(); i++) {

							TagPosition tagPosition = tagPositionListFromDB.get(i);
							HashMap<String, Object> posMap = new HashMap<>();
							posMap.put("x", Double.valueOf(tagPosition.getX()));
							posMap.put("y", Double.valueOf(tagPosition.getY()));
							posMap.put("z", Double.valueOf(tagPosition.getZ()));
							posMap.put("lastSeen", tagPosition.getLastSeen());
							posMap.put("mac", tagPosition.getMacId());
							posMap.put("zoneName", tagPosition.getZoneName());

							tagPositionMap.add(posMap);
						}
						if (tagPositionMap != null && tagPositionMap.size() > 0)
							sendTagPositionAsyncTask.sendTagPostionAsyncTask(tagPositionMap);


					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				} catch (Exception e) {
					addLogs("Error", "Genie", "Send Quuppa Tag Positions-Background", e.getMessage());
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		executorService.shutdown();  
	}

	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);
	}
}
