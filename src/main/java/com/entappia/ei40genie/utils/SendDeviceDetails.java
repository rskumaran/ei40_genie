package com.entappia.ei40genie.utils;

import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.dbmodels.FirmwareSettings;
import com.entappia.ei40genie.entappia.EntappiaApiService;
import com.entappia.ei40genie.quuppa.QuuppaApiService;
import com.entappia.ei40genie.repository.FirmwareSettingsRepository;
import com.sun.management.OperatingSystemMXBean;

@Component
public class SendDeviceDetails {

	@Autowired
	Logger logger;
	
	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	private EntappiaApiService entappiaApiService;

	@Autowired
	private FirmwareSettingsRepository firmwareSettingsRepository;
	
	@Autowired
	private EntappiaGenieApplication entappiaGenieApplication;

	Preference preference = new Preference();

	public void sendDeviceDetails() {
		try {

			if (!EntappiaGenieApplication.isServerConnected)
			{
				entappiaGenieApplication.getSessionData();
				addLogs("Error", "Genie", "sendDeviceDetails- Get AsyncTask",  "isServerConnected: FALSE");
				return;
			}

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				@SuppressWarnings("deprecation")
				public void run() {
					if (!Utils.isEmptyString(entappiaApiService.getSessionValue())) {
						CompletableFuture<JSONObject> qpeCompletableFuture;
						try {
							qpeCompletableFuture = quuppaApiService.getQPEInfo();
							CompletableFuture.allOf(qpeCompletableFuture).join();

							if (qpeCompletableFuture.isDone()) {

								try {
									JSONObject jsonObject = qpeCompletableFuture.get();
									if (jsonObject != null) {
										String status = jsonObject.optString("status");
										if (AppConstants.print_log)
											System.out.println("getQPEStart status==" + status);

										if (!Utils.isEmptyString(status) && status.equals("success")) {
											if (AppConstants.print_log)
												System.out.println("getQPEStart success==" + status);

											long memoryFree = jsonObject.getLong("memoryFree");
											long memoryMax = jsonObject.getLong("memoryMax");
											long memoryUsed = jsonObject.getLong("memoryUsed");
											long memoryAllocated = jsonObject.getLong("memoryAllocated");
											double cpuLoad = jsonObject.getDouble("cpuLoad") / 100;
											long diskFree = jsonObject.getLong("diskFree");

											DecimalFormat df = new DecimalFormat("#.##");

											String qpeVersion = jsonObject.getString("qpeVersion");

											HashMap<String, Object> jvmmap = new HashMap<>();
											jvmmap.put("free", memoryFree);
											jvmmap.put("total", memoryMax);
											jvmmap.put("used", memoryUsed);
											jvmmap.put("allocated", memoryAllocated);
											jvmmap.put("cpuLoad", Double.valueOf(df.format(cpuLoad)));
											jvmmap.put("diskFree", diskFree);

											HashMap<String, Object> serverDetails = new HashMap<>();

											serverDetails.put("primary_server",
													preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME));
											serverDetails.put("secondary_server",
													preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME));

											if (EntappiaGenieApplication.isPrimaryServer)
												serverDetails.put("current_server",
														preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME)); 
											else
												serverDetails.put("current_server",
														preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME));

											OperatingSystemMXBean osBean = ManagementFactory
													.getPlatformMXBean(OperatingSystemMXBean.class);

											long total = osBean.getTotalPhysicalMemorySize();
											long free = osBean.getFreePhysicalMemorySize();
											long used = total - free;

											HashMap<String, Object> systemMemory = new HashMap<>();
											systemMemory.put("free", free);
											systemMemory.put("total", total);
											systemMemory.put("used", used);

											HashMap<String, Object> map = new HashMap<>();

											HashMap<String, Object> versionmmap = new HashMap<>();
											Iterable<FirmwareSettings> firmWare = firmwareSettingsRepository.findAll();

											if (firmWare != null && firmWare.iterator().hasNext()) {
												for (FirmwareSettings firmWareData : firmWare) {
													versionmmap.put(firmWareData.getApplication(), firmWareData.getVersion());
												}
											} else {
												versionmmap.put("ei40", "");
												versionmmap.put("genie", "");
												versionmmap.put("qpe", "");
												versionmmap.put("asset", "");
												versionmmap.put("material", "");
												versionmmap.put("staff", "");
												versionmmap.put("vehicle", "");
												versionmmap.put("visitor", "");
												
											}

											

											map.put("jvmMemory", jvmmap);
											map.put("systemMemory", systemMemory);
											map.put("serverDetails", serverDetails);
											map.put("versionDetails", versionmmap);

											//map.put("genieLogCount", genieLogsRepository.count());
											//ToDo:Shiva
											//map.put("ctProfileLogCount", logCtProfileRepository.count());

											//alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_QPE);
											addLogs("Event", "Genie", "Get QPE Info", "Success");

											if (AppConstants.print_log)
												System.out.println("Send Device Details" + map);

											CompletableFuture<JSONObject> sendCompletableFuture = entappiaApiService
													.sendQuuppaDeviceDetails(map);
											CompletableFuture.allOf(sendCompletableFuture).join();
											if (qpeCompletableFuture.isDone()) {
												JSONObject jsonObject1 = sendCompletableFuture.get();
												if (jsonObject1 != null) {
													String status2 = jsonObject1.optString("status");
													if (!Utils.isEmptyString(status2)
															&& status2.equalsIgnoreCase("success")) {

														/*alertsRepository.deleteByType(
																AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA);*/
														addLogs("Event", "Genie",
																"Send Device Details-EntappiaGenieApp", "Success");
													} else {
														String message = jsonObject.optString("message");
														addLogs("Error", "Genie",
																"Send Device Details-EntappiaGenieApp", message);
														/*alertsRepository.addAlerts(
																AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA, message);*/
													}

												}
											}
										} else {
											if (AppConstants.print_log)
												System.out.println("getQPEStart error==" + status);

											String message = jsonObject.optString("message");
											addLogs("Error", "Genie", "Get QPE Info", message);
											//alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_QPE, message);
										}
									}
								} catch (ExecutionException e) {
									addLogs("Error", "Genie", "Get QPE Info", e.getMessage());
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						addLogs("Error", "Genie", "sendDeviceDetails- Get AsyncTask", "Session key is empty");
					}
				}
			});
			executorService.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);
	}
}
