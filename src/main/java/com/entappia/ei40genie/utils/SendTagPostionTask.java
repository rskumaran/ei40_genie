package com.entappia.ei40genie.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.dbmodels.TagPosition;
import com.entappia.ei40genie.repository.TagPositionRepository;

@Component
public class SendTagPostionTask {

	@Autowired
	Logger logger;
	
	private static ThreadPoolTaskScheduler sendTagPostionTaskcheduler;
	private static boolean isRunning;

	@Autowired
	TagPositionRepository tagPositionRepository;

	@Autowired
	private SendTagPositionAsyncTask sendTagPositionAsyncTask;

	@Autowired
	private EntappiaGenieApplication entappiaGenieApplication;

	Preference preference = new Preference();

	public void createSchedule() {

		if (sendTagPostionTaskcheduler == null) {
			sendTagPostionTaskcheduler = new ThreadPoolTaskScheduler();
			sendTagPostionTaskcheduler.setPoolSize(5);
			sendTagPostionTaskcheduler.setBeanName("asyncExecutor");
			sendTagPostionTaskcheduler.initialize();
		}

		startSchedule();
	}

	private void startSchedule() {
 
		if (!isRunning) {
			
			String cronTime = "0/" + preference.getIntValue(AppConstants.CONFIG_PREF_LTT, 20) + " * * * * *";

		if (AppConstants.print_log)
			System.out.println("SendTagPostionTask cronTime: " + cronTime);
			
			if (sendTagPostionTaskcheduler != null)
				sendTagPostionTaskcheduler.schedule(new RunnableTask(), new CronTrigger(cronTime));

			isRunning = true;
		}
	}

	public void stopSchedule() {
		if (sendTagPostionTaskcheduler != null) {
			sendTagPostionTaskcheduler.getScheduledThreadPoolExecutor().shutdown();
			sendTagPostionTaskcheduler.shutdown(); 
		}

		sendTagPostionTaskcheduler = null;
		isRunning = false;
	}

	public void changeScheduleTime() {
		stopSchedule();
		createSchedule();
		startSchedule();
	}

	class RunnableTask implements Runnable {

		public RunnableTask() {
		}

		@SuppressWarnings("unused")
		@Override
		public void run() {



			if (!EntappiaGenieApplication.isServerConnected) {
				entappiaGenieApplication.getSessionData();
				addLogs("Error", "Genie", "SendTagPostionTask", "isServerConnected: FALSE");
				return;
			}
			//ToDo:Shiva
			//Get tag details from tags repository and send

			try {
				List<TagPosition> tagPositionListFromDB  = tagPositionRepository.findAll();

				List<HashMap<String, Object>> tagPositionMap = new ArrayList<>();
				for (int i = 0; i < tagPositionListFromDB.size(); i++) {

					TagPosition tagPosition = tagPositionListFromDB.get(i);
					HashMap<String, Object> posMap = new HashMap<>();
					posMap.put("x", Double.valueOf(tagPosition.getX()));
					posMap.put("y", Double.valueOf(tagPosition.getY()));
					posMap.put("z", Double.valueOf(tagPosition.getZ()));
					posMap.put("zoneName", tagPosition.getZoneName());
					posMap.put("lastSeen", tagPosition.getLastSeen());
					posMap.put("mac", tagPosition.getMacId());
					
				 

					tagPositionMap.add(posMap);
				}
				if (tagPositionMap != null && tagPositionMap.size() > 0)
					sendTagPositionAsyncTask.sendTagPostionAsyncTask(tagPositionMap);


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);
	}

}
/* if tag positions need to be send by fetching from qpe then use this code
QuuppaApiService quuppaApiService = new QuuppaApiService();
CompletableFuture<JSONObject> getTagCompletableFuture;
try {
	getTagCompletableFuture = quuppaApiService.getTagDetails();

	CompletableFuture.allOf(getTagCompletableFuture).join();

	if (getTagCompletableFuture.isDone()) {
		JSONObject jsonObject = getTagCompletableFuture.get();
		if (jsonObject != null) {

			String status = jsonObject.optString("status");

			if (!Utils.isEmptyString(status)) {
				if (status.equals("success")) {

					JSONObject jsonTagObject = jsonObject.optJSONObject("response");
					if (jsonTagObject != null) {
						int code = jsonTagObject.getInt("code");
						if (code == 0) {

							//alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_QPE);
							JSONArray tagsJsonArray = jsonTagObject.getJSONArray("tags");
							if (tagsJsonArray != null) {

								HashMap<String, Object> tagPositionMap = new HashMap<String, Object>();

								DecimalFormat df = new DecimalFormat("#.##");
								for (int i = 0; i < tagsJsonArray.length(); i++) {
									JSONObject tagJsonObject = tagsJsonArray.getJSONObject(i);
									String id = tagJsonObject.optString("tagId", "");
									JSONArray smoothedPosition = tagJsonObject
											.optJSONArray("location");

									if (smoothedPosition != null) {
										double x = smoothedPosition.getDouble(0);
										double y = smoothedPosition.getDouble(1);
										double z = smoothedPosition.getDouble(2);

										double[] arr = Utils.getPoint(x, y);
										if (arr != null) {

											HashMap<String, Object> posMap = new HashMap<>();
											posMap.put("x", Double.valueOf(df.format(arr[0])));
											posMap.put("y", Double.valueOf(df.format(arr[1])));
											posMap.put("z", z);
											posMap.put("lastSeen", tagJsonObject.optLong("locationTS", 0));

											tagPositionMap.put(id, posMap);
										}

									}
								}

								if (tagPositionMap != null && tagPositionMap.size() > 0)
									sendTagPositionAsyncTask.sendTagPostionAsyncTask(tagPositionMap);
							}

						} else {
							String message = jsonTagObject.optString("message");
							//alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_QPE, message);
						}
					}

				} else if (status.equals("error")) {
					String message = jsonObject.getString("message");
					//alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_QPE, message);

				}
			}

		}
	}
 */