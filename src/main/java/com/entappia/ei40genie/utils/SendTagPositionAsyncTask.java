package com.entappia.ei40genie.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;

import com.entappia.ei40genie.entappia.EntappiaApiService;


@Component
public class SendTagPositionAsyncTask {


	@Autowired
	private EntappiaApiService apiService;

	public SendTagPositionAsyncTask() {
		super();

	}

	public void sendTagPostionAsyncTask(List<HashMap<String, Object>> tagPositionMap) {

		if(!EntappiaGenieApplication.isServerConnected)
			return;
		
		CompletableFuture<JSONObject> response;
		try {

			response = apiService.sendTagPositionDetails(tagPositionMap);
			CompletableFuture.allOf(response).join();
			if (response.isDone()) {
				JSONObject jsonObj = response.get();
				if (jsonObj != null) {
					if (AppConstants.print_log)
						System.out.println("Send TagPostion :-" + jsonObj);
					String status = jsonObj.optString("status");
					if (!Utils.isEmptyString(status) && status.equals("success")) {
						//ToDo:Shiva
						//alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA);
						
					} else {
						//ToDo:Shiva
						//alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA,
							//	jsonObj.optString("message"));
					}

				}

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
