package com.entappia.ei40genie.utils;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.RestErrorHandler;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.entappia.EntappiaApiService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component
public class SendAnalyticsTask {
	
	@Autowired
	Logger logger;
	
	@Autowired
	private EntappiaApiService entappiaApiService;
	
	Preference preference = new Preference();

	SendAnalyticsTask(){
		
	}
	/*
	public static String EI40_PEOPLE_DWELL_TIME_API = "peopleDwellTime";
	public static String EI40_VISITOR_DWELL_TIME_API = "visitorDwellTime";
	public static String EI40_ASSET_DWELL_TIME_API = "assetDwellTime";
	public static String EI40_WORK_STATION_FLOW_API = "WorkStationFlowAnalytics";
	public static String EI40_WORK_STATION_CONSTRAINT_API = "WorkStationConstraintAnalytics";
	public static String EI40_WORK_STATION_AGING_API = "WorkStationAgingAnalytics";
	public static String EI40_WORK_STATION_INVENTORY_API = "WorkStationInventory";
	public static String EI40_WORK_ORDER_TIME_API = "WorkOrderTimeAnalytics";
	public static String EI40_ZONE_FOOTFALL_API = "ZoneFootFall";
	public static String EI40_ZONE_ATTENDANCE_API = "attendance";
	
	*/
	public void getAnalyticsDataFromEI40Server(String requestType, HashMap<String, Object> requestParams )
	{
		try {

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {

					try {

						HashMap<String, Object> analyticsData =  new HashMap<>();
						analyticsData = frameAnalyticsData(requestType, requestParams);
						
						entappiaApiService.postServerCall(analyticsData, "/ei40AnalyticsData");

					} catch (InterruptedException e) {
						
						e.printStackTrace();
					}
					catch (JSONException e) {
						addLogs("Error", "Genie", "Send Zone Info-EntappiaGenieApp", e.getMessage());
						e.printStackTrace();
					}
				}
			});
			executorService.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Override timeouts in request factory
		private HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory() {
			HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
			// Connect timeout
			clientHttpRequestFactory.setConnectTimeout(50000);

			// Read timeout
			clientHttpRequestFactory.setReadTimeout(60000);
			return clientHttpRequestFactory;
		}

		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	HashMap<String, Object> frameAnalyticsData(String analyticsType, HashMap<String, Object> requestMap){
		HashMap<String, Object> analyticsData =  new HashMap<>();

		if (Utils.isEmptyString(getSessionValue())) {
			try {
				createSession();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		String ei40Url = getEI40ServerUrl(analyticsType);
		
		
		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		// build the request
		HttpEntity<HashMap<String, Object>> entity = new HttpEntity<>(requestMap, headers);
		
		try {
			
			if(analyticsType.equals("getLogs"))
			{
				ResponseEntity<String> response = restTemplate.postForEntity(ei40Url, entity, String.class);

				String logs = response.getBody();
				 if (response.getStatusCodeValue() == 200) {
						setSessionTime();
						//analyticsData.put("data", response.getBody());
						Gson gson = new Gson();
						Type listType = new TypeToken<List<HashMap<String, Object>>>() {
						}.getType();
						List<HashMap<String, Object>> analyticalResponse= gson.fromJson(logs, listType);
						
						analyticsData.put("data", analyticalResponse);
						return analyticsData;
					}
					
					else {
						if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
								|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
							if (response.getStatusCodeValue() == 403) {
								resetSessionValues();
							}

							try {
								createSession();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						analyticsData.put("status", "error");
						analyticsData.put("type", "Connection");
						analyticsData.put("message", "Connection Failed");
				}
			}
			else {
				 ResponseEntity<HashMap>  response = restTemplate.postForEntity(ei40Url, entity, HashMap.class);
				if (response.getStatusCodeValue() == 200) {
					setSessionTime();
					analyticsData.put("data", response.getBody());
					return analyticsData;
				}
				
				else {
					if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
							|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
						if (response.getStatusCodeValue() == 403) {
							resetSessionValues();
						}

						try {
							createSession();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					analyticsData.put("status", "error");
					analyticsData.put("type", "Connection");
					analyticsData.put("message", "Connection Failed");
			}
			
			

			}
		} catch (ResourceAccessException e) {
			analyticsData.put("status", "error");
			analyticsData.put("type", "Connection");
			analyticsData.put("message", "Resource Access Exception");
		}catch (Exception e) {
			analyticsData.put("status", "error");
			analyticsData.put("type", "Generic");
			analyticsData.put("message", "Resource Access Exception");
		}
				
		return analyticsData;
}
	private String getEI40ServerUrl(String analyticsType) {
		return AppConstants.EI40_LOCAL_SERVER_PATH + analyticsType;
	}
	@Async
	public void createSession() throws InterruptedException {

		String sessionUrl = getEI40ServerUrl(AppConstants.EI40_LOCAL_SERVER_LOGIN_API );

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		
		HashMap<String, Object> loginData = new HashMap<>();
		loginData.put("emailID", "admin@ei.com");
		loginData.put("password", "Entappia123");
		loginData.put("userType", "genie");
		
		HttpEntity<HashMap<String, Object>> entity = new HttpEntity<>(loginData, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(sessionUrl, HttpMethod.POST, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();
				String res = response.getBody();

				if (!Utils.isEmptyString(res)) {

					JSONObject jsonObject2 = new JSONObject(res);

					String status = jsonObject2.optString("status");

					if (!Utils.isEmptyString(status) && status.equalsIgnoreCase("success")) {
						HttpHeaders responseHeaders = response.getHeaders();
						String cookie = responseHeaders.getFirst(HttpHeaders.SET_COOKIE);

						String[] cookieArray = cookie.split(";");
						if (cookieArray != null && cookieArray.length > 0) {
							for (String key : cookieArray) {
								if (key.startsWith("JSESSIONID=")) {
									String sessionId = key;
									if (AppConstants.print_log)
										System.out.println("sessionId:" + sessionId);
									saveSessionValue(sessionId);
									jsonObject.put("sessionId", sessionId);
								}
							}
						}

						String organization = jsonObject2.optString("organization");

						jsonObject.put("organization", organization);
						jsonObject.put("status", "success");
					} else {
						String message = jsonObject2.optString("message");

						jsonObject.put("status", "error");
						jsonObject.put("message", message);
					}

				} else {
					jsonObject.put("status", "error");
					jsonObject.put("message", "Connection Failed");
				}

			} else {
				if (AppConstants.print_log) {
					System.out.println("Status Code " + response.getStatusCodeValue());
				}
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");
			}

		} catch (ResourceAccessException e) {

			if (AppConstants.print_log)
				System.out.println("message " + "Resource Access Exception");

			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}

	}

	public void resetSessionValues() {

		preference.setLongValue("EI40_" + AppConstants.SESSION_TIME, 0);
		preference.setStringValue("EI40_" + AppConstants.SESSION_KEY, "");


	}
	
	public void setSessionTime() {

		preference.setLongValue("EI40_" + AppConstants.SESSION_TIME, System.currentTimeMillis());

	}
	
	public long getSessionTime() {

		return preference.getLongValue("EI40_" + AppConstants.SESSION_TIME);
 
	}
	
	public void saveSessionValue(String sessionId) {

		preference.setStringValue("EI40_" + AppConstants.SESSION_KEY, sessionId);

	}
	
	public String getSessionValue() {

		return preference.getStringValue("EI40_" + AppConstants.SESSION_KEY);
 
	}
	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);
	}
}
