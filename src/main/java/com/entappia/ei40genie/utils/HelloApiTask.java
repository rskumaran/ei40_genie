package com.entappia.ei40genie.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.dbmodels.FirmwareSettings;
import com.entappia.ei40genie.models.HelloApiData;
import com.entappia.ei40genie.repository.FirmwareSettingsRepository;


@Component
public class HelloApiTask {

	@Autowired
	Logger logger;
	
	private static ThreadPoolTaskScheduler helloApiTaskScheduler;
	private static boolean isRunning;

	public String TAG_NAME = "HelloApiTask";

	Preference preference = new Preference();

	@Autowired
	private EntappiaGenieApplication entappiaGenieApplication;

	@Lazy
	@Autowired
	private HelloApiAsyncTask helloApiAsyncTask;

	@Autowired
	private FirmwareSettingsRepository firmwareSettingsRepository;
	
	@Autowired
	private HelloApiDataConstruct helloApiDataConstruct ;
	
	private HelloApiData helloApiData = new HelloApiData();

	public static Date tagOccupancyLastSendDate = null;

	public void createSchedule() {

		if (helloApiTaskScheduler == null) {
			helloApiTaskScheduler = new ThreadPoolTaskScheduler();
			helloApiTaskScheduler.setPoolSize(5);
			helloApiTaskScheduler.initialize();
		}

		startSchedule();
	}

	public void startSchedule() {

		String cronTime = "0 0/" + preference.getIntValue(AppConstants.CONFIG_PREF_HA, 1) + " * * * *";
		if (AppConstants.print_log)
			System.out.println("HelloApiTask cronTime: " + cronTime);

		if (!isRunning) {
			if (helloApiTaskScheduler != null)
				helloApiTaskScheduler.schedule(new RunnableTask("PeriodicTrigger"), new CronTrigger(cronTime));

			isRunning = true;
		}
	}

	public void stopSchedule() {
		if (helloApiTaskScheduler != null) {
			helloApiTaskScheduler.getScheduledThreadPoolExecutor().shutdown();
			helloApiTaskScheduler.shutdown();
		}

		helloApiTaskScheduler = null;
		isRunning = false;
	}

	public void changeScheduleTime() {
		stopSchedule();
		createSchedule();
		startSchedule();
	}

	class RunnableTask implements Runnable {
		private String message;

		public RunnableTask(String message) {
			this.message = message;
		}

		@SuppressWarnings("unused")
		@Override
		public void run() {

			try {

				if (!EntappiaGenieApplication.isServerConnected) {
					entappiaGenieApplication.getSessionData();
					addLogs("Error", "Genie", "HelloApiTask", "SESSION_KEY is empty");
					return;
				}

				String sDate = Utils.getDate(new Date());
				
				String lastResetDate = preference.getStringValue("entappia_downloadedFailedCountResetDate", "");
				
				if(!sDate.equals(lastResetDate)) {
					firmwareSettingsRepository.findAll().forEach(e -> { 
						preference.setIntValue(e.getApplication()
								+ "_downloadedFailedCount", 0);
						preference.setStringValue(e.getApplication()
								+ "_downloadedAppVersion", "");
						
					}); 
					
					preference.setStringValue("entappia_downloadedFailedCountResetDate", sDate);
				}
		
				HashMap<String, Object> data = new HashMap<>();
				
				helloApiDataConstruct.FrameHelloApiData();
				if(helloApiDataConstruct.getTagDetails() != null) {
					data.put("tagDetails", helloApiDataConstruct.getTagDetails());
				}
				
				if(helloApiDataConstruct.getTagColorDetails() != null) {
					data.put("tagColorDetails", helloApiDataConstruct.getTagColorDetails());
				}
				
				if(helloApiDataConstruct.getRemoveZoneDetails() != null) {
					data.put("removeZoneDetails", helloApiDataConstruct.getRemoveZoneDetails());
				}
				
				if(helloApiDataConstruct.getAddZoneDetails() != null) {
					data.put("addZoneDetails", helloApiDataConstruct.getAddZoneDetails());
				}
				
				if(helloApiDataConstruct.getApplicationVersion() != null) {
					data.put("appVersions", helloApiDataConstruct.getApplicationVersion());
				}
				
				if(helloApiDataConstruct.getRemoveMapDetails() != null) {
					data.put("removeMapDetails", helloApiDataConstruct.getRemoveMapDetails());
				}
				
				if(helloApiDataConstruct.getMapDetails() != null) {
					data.put("mapDetails", helloApiDataConstruct.getMapDetails());
				}
				
				if(helloApiDataConstruct.getOutlines() != null) {
					data.put("outlineDetailsList", helloApiDataConstruct.getOutlines());
				}
				
				if(helloApiDataConstruct.getPartitions() != null) {
					data.put("partitionsList", helloApiDataConstruct.getPartitions());
				}
				
				if(helloApiDataConstruct.getPathways() != null) {
					data.put("pathwayList", helloApiDataConstruct.getPathways());
				}
				
				if(helloApiDataConstruct.getDoors() != null) {
					data.put("doorsList", helloApiDataConstruct.getDoors());
				}
				
				if(helloApiDataConstruct.getWalls() != null) {
					data.put("wallsList", helloApiDataConstruct.getWalls());
				}
								
				helloApiAsyncTask.addHelloApiAsyncTask(data);

			} catch (Exception e) {
				addLogs("Error", "Genie", "Send HelloApi-Task", e.getMessage());

				e.printStackTrace();
			}

		}
	};

	
	
	@Async
	public void firmWareDownload(String path) throws InterruptedException, IOException {
		// TODO Auto-generated method stub

		try {

			ExecutorService executorService = Executors.newFixedThreadPool(2);
			executorService.execute(new Runnable() {

				public void run() {
					ResourceBundle bundle = ResourceBundle.getBundle("application");
					String dir = bundle.getString("linuxpath");

					String downloadUrl = AppConstants.EI40_DOWNLOAD_API;
					RestTemplate restTemplate = new RestTemplate();
					HttpHeaders headers = new HttpHeaders();
					headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
					HttpEntity<String> request = new HttpEntity<>(headers);

					byte[] response1 = restTemplate.getForObject(downloadUrl + "/" + path, byte[].class);
					try {
						Files.write(Paths.get(dir + "/" + path), response1);
						addLogs("Event", "Genie", "FirwareDownload-Hello Api", "Success");

					} catch (IOException e) {
						addLogs("Error", "Genie", "FirwareDownload-Hello Api", e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			addLogs("Error", "Genie", "FirwareDownload-Hello Api", e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);
	}
}
