package com.entappia.ei40genie.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.RestErrorHandler;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.dbmodels.FirmwareSettings;
import com.entappia.ei40genie.entappia.EntappiaApiService;
import com.entappia.ei40genie.repository.FirmwareSettingsRepository;
import com.entappia.ei40genie.models.ApplicationConfigurationSettings;

@Component
public class HelloApiAsyncTask {

	@Autowired
	Logger logger;
	
	static List<HashMap<String, Object>> helloApiQueue = new ArrayList<HashMap<String, Object>>();
	static boolean isHelloApiAsyncTaskCalled = true;

	Preference preference = new Preference();

	@Autowired
	private SendTagPostionTask sendingTagPostionTask;

	@Autowired
	private LocatorStatusTask locatorStatusTask;

	@Autowired
	private FirmwareSettingsRepository firmwareSettingsRepository;

	@Lazy
	@Autowired
	private HelloApiTask helloApiTask;

	@Autowired
	private SendDeviceDetails sendDeviceDetailsTask;

	@Autowired
	private EntappiaApiService apiService;

	@Autowired
	private TagBackgroundTasks tagBackgroundTasks;

	@Autowired
	private ApplicationConfigurationSettings applicationConfigurationSettings;
	
	@Autowired
	private SendAnalyticsTask sendAnalyticsTask;

	// public static String downloadedAppVersion = "";

	public HelloApiAsyncTask() {
		super();
	}

	public void addHelloApiAsyncTask(HashMap<String, Object> data) {

		helloApiQueue.add(data);

		if (isHelloApiAsyncTaskCalled && EntappiaGenieApplication.isServerConnected) {

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				@SuppressWarnings("unchecked")
				public void run() {
					int runCount = 0;
					while (helloApiQueue.size() > 0) {

						isHelloApiAsyncTaskCalled = false;
						HashMap<String, Object> helloApiData = helloApiQueue.get(0);

						try {
							if (!EntappiaGenieApplication.isServerConnected) {
								break;
							}

							//Here we send data
							CompletableFuture<JSONObject> response = apiService.helloApiDetails(helloApiData);
							CompletableFuture.allOf(response).join();
							if (response.isDone()) {
								JSONObject jsonObj = null;

								try {
									jsonObj = response.get();
									if (jsonObj != null) {

										String status = jsonObj.optString("status");

										if (!Utils.isEmptyString(status)) {
											if (status.equals("success")) {

												/*alertsRepository
														.deleteByType(AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA);*/
												ArrayList<HashMap<String, String>> logList = null;

												if (helloApiData.containsKey("logGenie"))
													logList = (ArrayList<HashMap<String, String>>) helloApiData
															.get("logGenie");

												ArrayList<HashMap<String, String>> logCtList = null;
												if (helloApiData.containsKey("logCtProfile"))
													logCtList = (ArrayList<HashMap<String, String>>) helloApiData
															.get("logCtProfile");

												if (logList != null && logList.size() > 0) {
													FirmwareSettings logCount = firmwareSettingsRepository.findByApplication("genie");

													if (logCount == null) {
														logCount = new FirmwareSettings();
														logCount.setApplication("genie");
													}

													logCount.setLogNo(Integer
															.parseInt(logList.get(logList.size() - 1).get("log_id")));
													if (AppConstants.print_log)
														System.out.println("genie LogNo" + logCount.getLogNo());

													firmwareSettingsRepository.save(logCount);
												}
												
												JSONObject details = null;
												try {
													details = response.get();
												} catch (ExecutionException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}

												if (details != null) {
													
													
													JSONObject requestedData = details.optJSONObject("data");
													boolean sendDeviceDetils = false, is_tag_data_requested = false, tagTracking = false; 
													JSONArray firmwareArray = null;
													JSONObject requestedAnalyticsData  = null;
													JSONArray configDetails = null;
													
													if(requestedData!=null) {
													  sendDeviceDetils = requestedData.optBoolean("sendDeviceDetails", false);
													  is_tag_data_requested = requestedData.optBoolean("is_tag_data_requested", false);
													  tagTracking = requestedData.optBoolean("tagTracking", false);
													  firmwareArray = requestedData.optJSONArray("updatedAppVersions");
													  requestedAnalyticsData = requestedData.optJSONObject("requested_data");
													  configDetails =  requestedData.optJSONArray("configDetails");
													}
													
													JSONArray tagArray = details.optJSONArray("tagDetails");
													JSONArray zoneArray = details.optJSONArray("zoneDetails");
													 
													
													//Analytics
													
													if (sendDeviceDetils) {
														if (sendDeviceDetailsTask != null)
															sendDeviceDetailsTask.sendDeviceDetails();
													}
													
													
													if(tagTracking)
													{
														if (sendingTagPostionTask != null)
															sendingTagPostionTask.createSchedule();
													}else
													{
														if (sendingTagPostionTask != null)
															sendingTagPostionTask.stopSchedule(); 
													}

													
													
													if (is_tag_data_requested) {
														
														if (tagBackgroundTasks != null)
															tagBackgroundTasks.sendTagPositions();
													}

													if (configDetails != null && configDetails.length()>0) {
														applicationConfigurationSettings.setConfigDetails(configDetails.toString());
														
														if (helloApiTask != null) {
															helloApiTask.stopSchedule();
															helloApiTask.createSchedule();
														}
															
														
														if (locatorStatusTask != null)
															locatorStatusTask.createSchedule();
														
													}

													if (requestedAnalyticsData != null) {
														
														String requestType = requestedAnalyticsData.getString("type");
														JSONObject jsonRequestParams = requestedAnalyticsData.getJSONObject("params");
														HashMap<String, Object> requestParams = (HashMap<String, Object>) jsonRequestParams.toMap();
														
														sendAnalyticsTask.getAnalyticsDataFromEI40Server(requestType, requestParams);
													}
													
													
													if (!AppConstants.print_log)
														System.out.println("firmwareArray===" + firmwareArray);

													if (firmwareArray != null) {
														for (int i = 0; i < firmwareArray.length(); i++) {
															JSONObject jsonObject = firmwareArray.getJSONObject(i);
															String orgname = jsonObject.optString("organization");

															if (!orgname.isEmpty() && orgname != null) {

																String path = jsonObject.optString("path");
																FirmwareSettings firmWare = firmwareSettingsRepository
																		.findByApplication(
																				jsonObject.optString("vtype"));
																if (firmWare != null) {
																	String DownloadedVersion = firmWare
																			.getDownloadedVersion();

																	if (Utils.isEmptyString(DownloadedVersion)
																			|| !DownloadedVersion.contentEquals(
																					jsonObject.optString("vname"))) {

																		firmWare.setApplication(
																				jsonObject.optString("vtype"));
																		firmWare.setDownloadedVersion(
																				jsonObject.optString("vname"));
																		try {

																			if (AppConstants.print_log)
																				System.out.println("AppVersion:"
																						+ preference.getStringValue(
																								jsonObject.optString(
																										"vtype")
																										+ "_downloadedAppVersion"));

																			if (AppConstants.print_log)
																				System.out.println("FailedCount:"
																						+ preference.getIntValue(
																								jsonObject.optString(
																										"vtype")
																										+ "_downloadedFailedCount",
																								0));

																			if (!preference
																					.getStringValue(jsonObject
																							.optString("vtype")
																							+ "_downloadedAppVersion")
																					.equals(jsonObject
																							.optString("vname"))
																					|| (preference
																							.getLongValue(jsonObject
																									.optString("vtype")
																									+ "_oldfwtime") != 0
																							&& (preference.getLongValue(
																									jsonObject
																											.optString(
																													"vtype")
																											+ "_oldfwtime")) != (jsonObject
																													.optLong(
																															"fwtime"))))

																			{
																				preference.setStringValue(jsonObject
																						.optString("vtype")
																						+ "_downloadedAppVersion", "");
																				preference.setIntValue(jsonObject
																						.optString("vtype")
																						+ "_downloadedFailedCount", 0);

																			}

																			if (preference
																					.getStringValue(jsonObject
																							.optString("vtype")
																							+ "_downloadedAppVersion")
																					.equals("")
																					|| preference.getIntValue(jsonObject
																							.optString("vtype")
																							+ "_downloadedFailedCount",
																							0) < 3) {
																				preference.setLongValue(
																						jsonObject.optString("vtype")
																								+ "_oldfwtime",
																						jsonObject.optLong("_fwtime"));
																				firmWareDownload(path, orgname,
																						firmWare);
																			}
																		} catch (IOException e) {
																			e.printStackTrace();
																		}
																	}
																} else {
																	firmWare = new FirmwareSettings();
																	firmWare.setApplication(
																			jsonObject.optString("vtype"));
																	firmWare.setDownloadedVersion(
																			jsonObject.optString("vname"));

																	if (!preference
																			.getStringValue(
																					jsonObject.optString("vtype")
																							+ "_downloadedAppVersion")
																			.equals(jsonObject.optString("vname"))) {

																		preference.setStringValue(
																				jsonObject.optString("vtype")
																						+ "_downloadedAppVersion",
																				"");
																		preference.setIntValue(
																				jsonObject.optString("vtype")
																						+ "_downloadedFailedCount",
																				0);

																	}

																	if (preference
																			.getStringValue(
																					jsonObject.optString("vtype")
																							+ "_downloadedAppVersion")
																			.equals("")
																			|| preference.getIntValue(
																					jsonObject.optString("vtype")
																							+ "_downloadedFailedCount",
																					0) < 3)
																		firmWareDownload(path, orgname, firmWare);
																}

															}

														}
													}

													if (tagArray != null) {

														//ToDo:Shiva
														/*for (int i = 0; i < tagArray.length(); i++) {

															JSONObject jsonObject = tagArray.getJSONObject(i);

															if (tagRepository != null) {
																Tags tags = tagRepository
																		.findByMacId(jsonObject.getString("macId"));
																if (tags == null) {
																	tags = new Tags();
																	tags.setMacId(jsonObject.getString("macId"));
																}

																tags.setTagId(jsonObject.getString("tagId"));
																tags.setStatus(jsonObject.getString("status"));
																tags.setAssignmentNo(
																		jsonObject.getString("assignmentNo"));
																tags.setAssignTime(jsonObject.getLong("assignTime"));
																tags.setToTime(jsonObject.getLong("toTime"));
																tags.setModifiedTime(
																		jsonObject.getLong("modifiedTime"));
																tags.setCreatedTime(jsonObject.getLong("createdTime"));
																tagRepository.save(tags);

																alertsRepository
																		.deleteByMacId(jsonObject.getString("macId"));

																if (preference.getLongValue(
																		"tagLastModifiedTime") < jsonObject
																				.getLong("modifiedTime"))
																	preference.setLongValue("tagLastModifiedTime",
																			jsonObject.getLong("modifiedTime"));

															}
														}*/
													}
													if (zoneArray != null && zoneArray.length() > 0) {

														/*ArrayList<Zones> zoneList = (ArrayList<Zones>) Utils
																.getObjectList(zoneArray.toString(), Zones.class);
														if (zoneList != null && zoneList.size() > 0) {

															DecimalFormat df = new DecimalFormat("#.##");

															for (Zones zones : zoneList) {
																if (zones != null) {

																	String polygonData = zones.getPolygonData();
																	String[] polygonDataArray = polygonData
																			.split(Pattern.quote("|"));

																	ArrayList<String> pointList = new ArrayList<>();

																	for (int j = 0; j < polygonDataArray.length; j++) {
																		String coordinate = polygonDataArray[j];
																		String[] coordinates = coordinate.split(",");

																		double x1 = Double.valueOf(coordinates[0]);
																		double y1 = Double.valueOf(coordinates[1]);

																		double[] arr = Utils.getQuuppaPoint(x1, y1);

																		if (arr != null) {
																			pointList.add(df.format(arr[0]) + ","
																					+ df.format(arr[1]));
																		}
																	}

																	if (zoneRepository != null) {
																		Zones zone = zoneRepository
																				.findByZoneId(zones.getZoneId());
																		if (zone == null) {
																			zones.setPolygonData(
																					String.join("|", pointList));
																			zoneRepository.save(zones);
																		} else {
																			zones.setPolygonData(
																					String.join("|", pointList));
																			zones.setId(zone.getId());
																			zoneRepository.save(zones);
																		}

																		if (preference.getLongValue(
																				"zoneLastModifiedTime") < zones
																						.getModifiedTime())
																			preference.setLongValue(
																					"zoneLastModifiedTime",
																					zones.getModifiedTime());
																	}
																}
															}
														}*/
													}
												}
												addLogs("Event", "Genie", "Send HelloApi-AsyncTask", "Success");

												helloApiQueue.remove(0);
												runCount = 0;
											} else if (status.equals("error")) {
												String message = jsonObj.optString("message");
												addLogs("Error", "Genie", "Send HelloApi-AsyncTask", message);
												runCount++;

												/*alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA,
														message);*/

												boolean resourceAccessException = jsonObj
														.optBoolean("ResourceAccessException", false);
												if (resourceAccessException) {
													break;
												}
											}

										}
									}
								} catch (ExecutionException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									addLogs("Error", "Genie", "Send HelloApi-AsyncTask", e1.getMessage());
									runCount++;
								}

							}
						}

						catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							addLogs("Error", "Genie", "Send HelloApi-AsyncTask", e.getMessage());
							runCount++;
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							addLogs("Error", "Genie", "Send HelloApi-AsyncTask", e.getMessage());
							runCount++;
						} catch (KeyManagementException e2) {
							// TODO Auto-generated catch block
							addLogs("Error", "Genie", "Send HelloApi-AsyncTask", e2.getMessage());
							runCount++;
							e2.printStackTrace();
						} catch (JSONException e2) {
							// TODO Auto-generated catch block
							addLogs("Error", "Genie", "Send HelloApi-AsyncTask", e2.getMessage());
							e2.printStackTrace();
							runCount++;
						} catch (NoSuchAlgorithmException e2) {
							// TODO Auto-generated catch block
							addLogs("Error", "Genie", "Send HelloApi-AsyncTask", e2.getMessage());
							e2.printStackTrace();
							runCount++;
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							addLogs("Error", "Genie", "Send HelloApi-AsyncTask", e2.getMessage());
							e2.printStackTrace();
							runCount++;
						}
						if (runCount >= 3)
							break;
					}

					isHelloApiAsyncTaskCalled = true;
				}
			});
			executorService.shutdown();
		}

	}

	// Override timeouts in request factory
	private HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		// Connect timeout
		clientHttpRequestFactory.setConnectTimeout(300000);

		// Read timeout
		clientHttpRequestFactory.setReadTimeout(60000);
		return clientHttpRequestFactory;
	}

	@Async
	public void firmWareDownload(String path, String orgname, FirmwareSettings firmWare)
			throws InterruptedException, IOException {
// TODO Auto-generated method stub
// System.out.println("Firm ware update");

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				if (AppConstants.print_log)
					System.out.println("HelloAPI firmWareDownload:-" + path);

				ResourceBundle bundle = ResourceBundle.getBundle("application");
				String dir = Utils.getDownloadServerPath();

				String downloadUrl = apiService.getUrl(AppConstants.EI40_DOWNLOAD_API);
				RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
				restTemplate.setErrorHandler(new RestErrorHandler());
				HttpHeaders headers = new HttpHeaders();
				headers.add("Cookie", apiService.getSessionValue());
				headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

				HttpEntity<String> request = new HttpEntity<>(headers);

				try {

					ResponseEntity<byte[]> response1 = restTemplate.exchange(
							downloadUrl + "/" + orgname + "/firmware/" + path, HttpMethod.GET, request, byte[].class);

					/*
					 * byte[] response1 = restTemplate.getForObject(downloadUrl + "/" + orgname +
					 * "/firmware/" + path, byte[].class);
					 */

					if (response1.getStatusCodeValue() == 200) {
						try {
							/*
							 * Path storagePath = Paths.get(dir.); Files.c(storagePath);
							 */
							File folder = new File(dir);
							
							if (AppConstants.print_log)
								System.out.println("path===" + folder);
							if (!folder.exists()) {
								folder.mkdir();
								
							}
							File f = new File(dir + File.separator + path);
							if (f.createNewFile()) {

							}

							Files.write(Paths.get(dir + "/" + path), response1.getBody());
							addLogs("Event", firmWare.getApplication(), "FirwareDownload-Hello Api", "Success");
							preference.setIntValue(firmWare.getApplication() + "_downloadedFailedCount", 0);
							preference.setStringValue(firmWare.getApplication() + "_downloadedAppVersion", "");

							if (AppConstants.print_log)
								System.out.println("Firmware Downloaded Successfully");
							firmwareSettingsRepository.save(firmWare);

						} catch (IOException e) {

							setDownloadedFailedValues(firmWare);

							if (AppConstants.print_log)
								System.out.println("Firmware Failed count==" + preference
										.getIntValue(firmWare.getApplication() + "_downloadedFailedCount", 0));
							if (AppConstants.print_log)
								System.out.println("Firmware Downloaded Failed");
							addLogs("Error", firmWare.getApplication(), "FirwareDownload-Hello Api", e.getMessage());
							e.printStackTrace();
						}

					} else {

						if (response1.getBody() != null) {
							Object obj = new Object();

							ObjectInputStream bin;
							try {
								bin = new ObjectInputStream(new ByteArrayInputStream(response1.getBody()));
								obj = bin.readObject();
							} catch (IOException e) {
								e.printStackTrace();

							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							}
							if (obj != null) {
								HashMap<String, String> value = ((HashMap<String, String>) obj);
								if (value != null) {
									setDownloadedFailedValues(firmWare);

									if (AppConstants.print_log)
										System.out.println("FirwareDownload-Hello Api:Status Code: "
												+ response1.getStatusCodeValue() + " Message: " + value.get("message"));

									addLogs("Error", firmWare.getApplication(), "FirwareDownload-Hello Api",
											"Status Code: " + response1.getStatusCodeValue() + " Message: "
													+ value.get("message"));
								}

							}

						} else {
							setDownloadedFailedValues(firmWare);

							addLogs("Error", firmWare.getApplication(), "FirwareDownload-Hello Api",
									"Status Code: " + response1.getStatusCodeValue());
						}

					}

				} catch (Exception e) {
					setDownloadedFailedValues(firmWare);

					if (AppConstants.print_log)
						System.out.println("Firmware Downloaded Failed");
					addLogs("Error", firmWare.getApplication(), "FirwareDownload-Hello Api", e.getMessage());
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	public void setDownloadedFailedValues(FirmwareSettings settings) {
		preference.setIntValue(settings.getApplication() + "_downloadedFailedCount",
				preference.getIntValue(settings.getApplication() + "_downloadedFailedCount", 0) + 1);

		preference.setStringValue(settings.getApplication() + "_downloadedAppVersion", settings.getDownloadedVersion());
	}

	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);
	}
}
