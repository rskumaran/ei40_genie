package com.entappia.ei40genie.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.entappia.EntappiaApiService;
import com.entappia.ei40genie.quuppa.QuuppaApiService;
import com.google.gson.Gson;

@Component
public class LocatorStatusTask {

	@Autowired
	Logger logger;
	
	private static ThreadPoolTaskScheduler locatorStatusTaskScheduler;
	private static boolean isRunning;
	Preference preference = new Preference();

	@Autowired
	private EntappiaApiService entappiaApiService;

	@Autowired
	private EntappiaGenieApplication entappiaGenieApplication;

	public void createSchedule() {

		if (locatorStatusTaskScheduler == null) {
			locatorStatusTaskScheduler = new ThreadPoolTaskScheduler();
			locatorStatusTaskScheduler.setPoolSize(5);
			locatorStatusTaskScheduler.initialize();
		}

		startSchedule();
	}

	public void startSchedule() {

		String cronTime = "0 0/" + preference.getIntValue(AppConstants.CONFIG_PREF_LS, 15) + " * * * *";
		if (AppConstants.print_log)
			System.out.println("LocatorStatusTask cronTime: " + cronTime);

		if (!isRunning) {
			if (locatorStatusTaskScheduler != null)
				locatorStatusTaskScheduler.schedule(new RunnableTask("PeriodicTrigger"), new CronTrigger(cronTime));

			isRunning = true;
		}
	}

	public void stopSchedule() {
		if (locatorStatusTaskScheduler != null) {
			locatorStatusTaskScheduler.getScheduledThreadPoolExecutor().shutdown();
			locatorStatusTaskScheduler.shutdown();
		}

		locatorStatusTaskScheduler = null;
		isRunning = false;
	}

	public void changeScheduleTime() {
		stopSchedule();
		createSchedule();
		startSchedule();
	}

	class RunnableTask implements Runnable {
		private String message;

		public RunnableTask(String message) {
			this.message = message;
		}

		@SuppressWarnings("unused")
		@Override
		public void run() {

			if (!EntappiaGenieApplication.isServerConnected) {
				entappiaGenieApplication.getSessionData();
				addLogs("Error", "Genie", "LocatorStatusTask-Get AsyncTask", "isServerConnected: FALSE");
				return;
			}

			QuuppaApiService quuppaApiService = new QuuppaApiService();
			CompletableFuture<JSONObject> locatorInfoCompletableFuture;
			try {
				locatorInfoCompletableFuture = quuppaApiService.getLocatorInfo();
				CompletableFuture.allOf(locatorInfoCompletableFuture).join();
				if (locatorInfoCompletableFuture.isDone()) {

					JSONObject jsonObj = null;

					try {
						jsonObj = locatorInfoCompletableFuture.get();
						if (jsonObj != null) {

							String status = jsonObj.optString("status");

							if (!Utils.isEmptyString(status)) {
								if (status.equals("success")) {
									//alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_QPE);

									HashMap<String, Object> locatorMap = new HashMap<>();
									List<HashMap<String, String>> locatorStatusList = new ArrayList<>();

									JSONArray locatorArray = jsonObj.getJSONArray("locators");
									if (locatorArray != null) {

										;
										for (int i = 0; i < locatorArray.length(); i++) {
											JSONObject jsonObject = locatorArray.getJSONObject(i);
											if (jsonObject != null) {

												String connection = jsonObject.optString("connection");
												String id = jsonObject.optString("id");

												if (!Utils.isEmptyString(connection) && !Utils.isEmptyString(id)) {
													HashMap<String, Object> statusMap = new HashMap<>();
													statusMap.put("connection", connection);
													statusMap.put("time", System.currentTimeMillis());
													locatorMap.put(id, statusMap);

													if (!connection.equalsIgnoreCase("ok")) {

														HashMap<String, String> map = new HashMap<>();
														map.put("name", jsonObject.optString("name"));
														map.put("status", connection);
														map.put("id", id);
														locatorStatusList.add(map);
													}
												}
											}
										}

										if (locatorStatusList.size() > 0) {
											String locatorStatus = new Gson().toJson(locatorStatusList);
											/*alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_LOCATOR_STATUS,
													locatorStatus);*/
										} else {
											/*alertsRepository
													.deleteByType(AppConstants.ERROR_CONECTION_TYPE_LOCATOR_STATUS);*/
										}
									}

									if (locatorMap.size() > 0) {
										// locatorMap

										CompletableFuture<JSONObject> sendLocatorInfoCompletableFuture = entappiaApiService
												.updateLocatorStatus(locatorMap);
										CompletableFuture.allOf(sendLocatorInfoCompletableFuture).join();
										if (sendLocatorInfoCompletableFuture.isDone()) {
											JSONObject sendResponseObj = sendLocatorInfoCompletableFuture.get();

											if (sendResponseObj != null) {
												if (AppConstants.print_log)
													System.out.println(sendResponseObj);

												String sendStatus = sendResponseObj.optString("status");

												if (!Utils.isEmptyString(sendStatus)) {
													if (sendStatus.equals("success")) {
														/*alertsRepository.deleteByType(
																AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA);*/
														addLogs("Event", "Genie", "LocatorStatusTask-Send AsyncTask",
																"Locator status updated.");
													} else if (sendStatus.equals("error")) {
														String message = sendResponseObj.optString("message");
														addLogs("Error", "Genie", "LocatorStatusTask-Send AsyncTask",
																message);
														/*alertsRepository.addAlerts(
																AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA, message);*/
													}
												}
											}

										}

									}
								} else if (status.equals("error")) {
									String message = jsonObj.optString("message");
									addLogs("Error", "Genie", "LocatorStatusTask-Get AsyncTask", message);
									//alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_QPE, message);
								}
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						addLogs("Error", "Genie", "LocatorStatusTask-Get AsyncTask", e.getMessage());
					}

				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				addLogs("Error", "Genie", "LocatorStatusTask-Get AsyncTask", e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				addLogs("Error", "Genie", "LocatorStatusTask-Get AsyncTask", e.getMessage());
			}

		}
	};

	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);

	}
}
