package com.entappia.ei40genie.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.entappia.ei40genie.dbmodels.GenieLogs;
import com.entappia.ei40genie.repository.GenieLogsRepository;

@Service
public class Logger {

	List<GenieLogs> syssyncQueue = new ArrayList<GenieLogs>();
	boolean syslog = true;

	@Autowired
	GenieLogsRepository genieLogsRepository;
	
	@Async
	private void AddLog(GenieLogs logs) {
		try {
			syssyncQueue.add(logs);
			if (syslog) {
				while (syssyncQueue.size() > 0) {
					syslog = false;
					GenieLogs log = syssyncQueue.get(0);
								
					log.setDate(new Date());
					
					if (genieLogsRepository.save(log) != null) {
						syssyncQueue.remove(0);
					}
					
				}
				syslog = true;
			}

		} catch (

		Exception e) {
			// TODO: handle exception
		}

	}
	public void addLogs(String type, String major, String minor, String message) {
		GenieLogs logs = new GenieLogs();
		logs.setDate(new Date());
		logs.setType(type);
		logs.setMajor(major);
		logs.setMinor(minor);
		logs.setResult(message);
		AddLog(logs);
}

}
