package com.entappia.ei40genie.entappia;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.net.ssl.SSLContext;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.RestErrorHandler;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;

@Service
public class EntappiaApiService {

	Preference preference = new Preference();

	@Autowired
	EntappiaGenieApplication entappiaGenieApplication;

	// public static boolean isPrimaryServer = true;
	// public static int primaryServerErrorCount = 0;

	/*@SuppressWarnings("null")
	@Async
	public CompletableFuture<JSONObject> getZoneDetails() throws InterruptedException {

		String zoneUrl = getUrl(AppConstants.ENTAPPIA_ZONE);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);

		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);

		JSONObject jsonObject = new JSONObject();
		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(zoneUrl, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String zones = response.getBody();
				setSessionTime();
				JSONObject resJsonObject = new JSONObject(zones);

				if (resJsonObject != null) {
					String status = resJsonObject.optString("status");
					if (!Utils.isEmptyString(status) && status.equalsIgnoreCase("success")) {
						JSONArray json = resJsonObject.getJSONArray("zones");
						jsonObject.put("status", "success");
						jsonObject.put("response", json);
					} else {
						String message = resJsonObject.optString("message");
						jsonObject.put("status", "error");
						jsonObject.put("message", message);
					}
				}

			} else {

				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}
				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
			entappiaGenieApplication.getSessionData();
		}
		return CompletableFuture.completedFuture(jsonObject);
	}
*/
	@Async
	public CompletableFuture<JSONObject> checkSession() throws InterruptedException {
	
		String sessionURL = getUrl(AppConstants.EI40_CHECK_SESSION);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		JSONObject jsonObject = new JSONObject();

		try { 
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(sessionURL, HttpMethod.GET, request, String.class); 
			if (response.getStatusCodeValue() == 200) {
				jsonObject.put("status", "success");
			} else {
				jsonObject.put("status", "error");

			}
		} catch (ResourceAccessException e) {
			System.out.println("TimeOut3: " + System.currentTimeMillis());
			jsonObject.put("status", "error");
		}

		return CompletableFuture.completedFuture(jsonObject);
	}

	// Override timeouts in request factory
	private HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		// Connect timeout
		clientHttpRequestFactory.setConnectTimeout(50000);

		// Read timeout
		clientHttpRequestFactory.setReadTimeout(60000);
		return clientHttpRequestFactory;
	}

	@Async
	public CompletableFuture<JSONObject> createSession(String licenseKey) throws InterruptedException {

		String sessionUrl = getUrl(AppConstants.EI40_SESSION + licenseKey);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(sessionUrl, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();
				String res = response.getBody();

				if (!Utils.isEmptyString(res)) {

					JSONObject jsonObject2 = new JSONObject(res);

					String status = jsonObject2.optString("status");

					if (!Utils.isEmptyString(status) && status.equalsIgnoreCase("success")) {
						HttpHeaders responseHeaders = response.getHeaders();
						String cookie = responseHeaders.getFirst(HttpHeaders.SET_COOKIE);

						String[] cookieArray = cookie.split(";");
						if (cookieArray != null && cookieArray.length > 0) {
							for (String key : cookieArray) {
								if (key.startsWith("JSESSIONID=")) {
									String sessionId = key;
									if (AppConstants.print_log)
										System.out.println("sessionId:" + sessionId);
									jsonObject.put("sessionId", sessionId);
								}
							}
						}

						String organization = jsonObject2.optString("organization");

						jsonObject.put("organization", organization);
						jsonObject.put("status", "success");
					} else {
						String message = jsonObject2.optString("message");

						jsonObject.put("status", "error");
						jsonObject.put("message", message);
					}

				} else {
					jsonObject.put("status", "error");
					jsonObject.put("message", "Connection Failed");
				}

			} else {
				if (AppConstants.print_log) {
					System.out.println("Status Code " + response.getStatusCodeValue());
				}
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");
			}

		} catch (ResourceAccessException e) {

			if (AppConstants.print_log)
				System.out.println("message " + "Resource Access Exception");

			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}

		// }
		return CompletableFuture.completedFuture(jsonObject);
	}

	/*@Async
	public CompletableFuture<JSONObject> getTagsDetails() throws InterruptedException, JSONException {

		String zoneUrl = getUrl(AppConstants.ENTAPPIA_TAGS);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(zoneUrl, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {
				String tags = response.getBody();
				setSessionTime();
				JSONObject tagsObject = new JSONObject(tags);
				if (tagsObject != null) {
					String status = tagsObject.getString("status");

					if (!Utils.isEmptyString(status) && status.equalsIgnoreCase("success")) {
						JSONArray tagArray = tagsObject.getJSONArray("tags");
						jsonObject.put("status", "success");
						jsonObject.put("tagArray", tagArray);
					} else {
						jsonObject.put("status", "error");
						jsonObject.put("type", "Connection");
						jsonObject.put("message", "Connection Failed");
					}
				}

			} else {
				if (response.getStatusCodeValue() == 403) { 
					resetSessionValues();
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}*/

	/*@SuppressWarnings("null")
	@Async
	public CompletableFuture<JSONObject> sendQuuppaDetails(HashMap<String, Object> quuppadata)
			throws InterruptedException {
		String url = getUrl(AppConstants.ENTAPPIA_DATA);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(quuppadata, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();
				jsonObject.put("status", "success");
				return CompletableFuture.completedFuture(jsonObject);

			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Entappia Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}*/

	@SuppressWarnings("null")
	@Async
	public CompletableFuture<JSONObject> sendTagPositionDetails(List<HashMap<String, Object>> quuppadata)
			throws InterruptedException {

		String url = getUrl(AppConstants.EI40_UPDATE_LIVE_TAGS);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity<List<HashMap<String, Object>>> entity = new HttpEntity<>(quuppadata, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				String sResponse = response.getBody();
				if (!Utils.isEmptyString(sResponse)) {

					JSONObject resObject = new JSONObject(sResponse);
					if (resObject != null) {
						String status = resObject.optString("status");
						if (status != null && status.equalsIgnoreCase("success")) {
							jsonObject.put("status", "success");
						} else {
							String message = resObject.optString("message");
							jsonObject.put("status", "error");
							jsonObject.put("message", message);
						}
					}
				}

				return CompletableFuture.completedFuture(jsonObject);

			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
					if (response.getStatusCodeValue() == 403) {
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	@Async
	public CompletableFuture<JSONObject> helloApiDetails(HashMap<String, Object> data)
			throws InterruptedException, JSONException, KeyManagementException, NoSuchAlgorithmException {

		String url = getUrl(AppConstants.EI40_HELLO_API);

		SSLContext context = SSLContext.getInstance("TLSv1.2");
		context.init(null, null, null);

		CloseableHttpClient httpClient = HttpClientBuilder.create().setSSLContext(context).build();
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
		RestTemplate restTemplate = new RestTemplate(factory);

		// RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject jsonObject = null;

		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
			if (AppConstants.print_log)
				System.out.println("HelloAPI Response Code:-" + response.getStatusCodeValue());
			if (response.getStatusCodeValue() == 200) {
				setSessionTime();
				String tags = response.getBody();
				if (!AppConstants.print_log)
					System.out.println("HelloAPI Response:-" + tags);
				jsonObject = new JSONObject(tags);
				jsonObject.put("status", "success");
			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject = new JSONObject();
				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
			jsonObject.put("ResourceAccessException", false);
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject = new JSONObject();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("ResourceAccessException", true);
			jsonObject.put("message", "Resource Access Exception");

		} catch (Exception e) {
			jsonObject = new JSONObject();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", e.getMessage());
			jsonObject.put("ResourceAccessException", false);
			e.printStackTrace();
		}

		return CompletableFuture.completedFuture(jsonObject);
	}

	/*@Async
	public CompletableFuture<JSONObject> sendAlerts(HashMap<String, Object> data)
			throws InterruptedException, JSONException, KeyManagementException, NoSuchAlgorithmException {

		String url = getUrl(AppConstants.ENTAPPIA_SEND_ALERT);

		SSLContext context = SSLContext.getInstance("TLSv1.2");
		context.init(null, null, null);

		CloseableHttpClient httpClient = HttpClientBuilder.create().setSSLContext(context).build();
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
		RestTemplate restTemplate = new RestTemplate(factory);

		// RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject jsonObject = null;

		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
			if (response.getStatusCodeValue() == 200) {
				setSessionTime();
				String tags = response.getBody();
				jsonObject = new JSONObject(tags);
				jsonObject.put("status", "success");
			} else {

				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject = new JSONObject();
				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Entappia API Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject = new JSONObject();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}

		return CompletableFuture.completedFuture(jsonObject);
	}*/

	/*@Async
	public CompletableFuture<JSONObject> projectInfoDetails(HashMap<String, Object> data)
			throws InterruptedException, JSONException {

		String url = getUrl(AppConstants.EI40_PROJECT_INFO);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);

		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				jsonObject.put("status", "success");
				return CompletableFuture.completedFuture(jsonObject);
			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}*/

	
	@Async
	public CompletableFuture<JSONObject> postServerCall(HashMap<String, Object> data, String url)
			throws InterruptedException, JSONException {


		String hostURL = getUrl(url);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);

		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(hostURL, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				jsonObject.put("status", "success");
				return CompletableFuture.completedFuture(jsonObject);
			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject); 
	}
	
	/*@Async
	public CompletableFuture<JSONObject> sendMapDetails(HashMap<String, Object> data)
			throws InterruptedException, JSONException {

		String url = getUrl(AppConstants.EI40_MAP_DETAILS);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);

		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				jsonObject.put("status", "success");
				return CompletableFuture.completedFuture(jsonObject);
			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}
	
	
	@Async
	public CompletableFuture<JSONObject> sendZoneDetails(HashMap<String, Object> data)
			throws InterruptedException, JSONException {

		String url = getUrl(AppConstants.EI40_ZONE_DETAILS);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);

		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				jsonObject.put("status", "success");
				return CompletableFuture.completedFuture(jsonObject);
			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}
	
	
	@Async
	public CompletableFuture<JSONObject> sendConfigurationDetails(HashMap<String, Object> data)
			throws InterruptedException, JSONException {

		String url = getUrl(AppConstants.EI40_TAG_COLOR_DETAILS);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);

		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				jsonObject.put("status", "success");
				return CompletableFuture.completedFuture(jsonObject);
			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503) {
					if (response.getStatusCodeValue() == 403) { 
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}*/
	
	public CompletableFuture<JSONObject> sendTagsDetails(List<HashMap<String, Object>> tagList) {
		// TODO Auto-generated method stub
		String url = getUrl(AppConstants.EI40_TAGS);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);

		@SuppressWarnings("rawtypes")
		HttpEntity<List<HashMap<String, Object>>> entity = new HttpEntity<>(tagList, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				jsonObject.put("status", "success");
				return CompletableFuture.completedFuture(jsonObject);
			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
					if (response.getStatusCodeValue() == 403) {
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	@SuppressWarnings("null")
	@Async
	public CompletableFuture<JSONObject> updateLocatorStatus(HashMap<String, Object> quuppadata)
			throws InterruptedException {
		//TODO SHIVA
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", "error");
		jsonObject.put("type", "Connection");
		jsonObject.put("message", "Connection Failed");
		return CompletableFuture.completedFuture(jsonObject);
		/*String url = getUrl(AppConstants.ENTAPPIA_LOCATOR_STATUS);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(quuppadata, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				jsonObject.put("status", "success");
				String sResponse = response.getBody();
				if (!Utils.isEmptyString(sResponse)) {
					jsonObject.put("response", new JSONObject(sResponse));
				}

				return CompletableFuture.completedFuture(jsonObject);

			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503) {
					if (response.getStatusCodeValue() == 403) {
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);*/
	}

	@Async
	public CompletableFuture<JSONObject> getProfileConfiguration() throws InterruptedException {

		String configurationUrl = getUrl(AppConstants.EI40_GET_CONFIGURATION);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);

		JSONObject jsonObject = new JSONObject();
		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(configurationUrl, HttpMethod.GET, request,
					String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				String configuration = response.getBody();
				if (!Utils.isEmptyString(configuration)) {

					JSONObject json = new JSONObject(configuration);
					jsonObject.put("response", json);

				}
				jsonObject.put("status", "success");
			} else {
				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
					if (response.getStatusCodeValue() == 403) {
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	@SuppressWarnings("null")
	@Async
	public CompletableFuture<JSONObject> sendQuuppaDeviceDetails(HashMap<String, Object> quuppadata)
			throws InterruptedException {

		String url = getUrl(AppConstants.EI40_UPDATE_DEVICE_DETAILS);

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(quuppadata, headers);
		JSONObject jsonObject = new JSONObject();

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			if (response.getStatusCodeValue() == 200) {
				setSessionTime();

				jsonObject.put("status", "success");
				return CompletableFuture.completedFuture(jsonObject);

			} else {

				if (response.getStatusCodeValue() == 403 || response.getStatusCodeValue() == 502
						|| response.getStatusCodeValue() == 503 || response.getStatusCodeValue() == 404) {
					if (response.getStatusCodeValue() == 403) {
						resetSessionValues();
					}

					if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY)))
						entappiaGenieApplication
								.createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
				}

				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Entappia Connection Failed");

			}
		} catch (ResourceAccessException e) {
			entappiaGenieApplication.getSessionData();
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", "Resource Access Exception");
		}
		return CompletableFuture.completedFuture(jsonObject);
	}

/*	@Async
	public CompletableFuture<Boolean> checkServerConnection(String hostName) {

		boolean hasServerConnected = false;
		String zoneUrl = hostName + AppConstants.ENTAPPIA_CHECK_SESSION;

		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", getSessionValue());
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);

		try {
			System.out.println("TimeOut1: " + System.currentTimeMillis());
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(zoneUrl, HttpMethod.GET, request, String.class);
			System.out.println("TimeOut2: " + System.currentTimeMillis());
			if (response.getStatusCodeValue() == 200) {
				hasServerConnected = true;
			}
		} catch (ResourceAccessException e) {
			System.out.println("TimeOut3: " + System.currentTimeMillis());
			hasServerConnected = false;
		}

		return CompletableFuture.completedFuture(hasServerConnected);

	}*/

	@Async
	public CompletableFuture<Boolean> isServerConncted(String hostName) {

		boolean isConnected = false;
		try {

			HttpURLConnection huc;
			URL url = new URL(hostName);
			if (AppConstants.print_log){
				System.out.println("Trying to connect server... " + url);
			}
			
			huc = (HttpURLConnection) url.openConnection();
			int responseCode = huc.getResponseCode();
			if (responseCode == 200) {
				isConnected = true;
			}
			if (AppConstants.print_log){
			System.out.println("Response code " + responseCode);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return CompletableFuture.completedFuture(isConnected);
	}

	public String getUrl(String url) {

		String host = "";
		if (EntappiaGenieApplication.isPrimaryServer)
			host = preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME) + url;
		else
			host = preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME) + url;
		if (AppConstants.print_log)
			System.out.println("getUrl " + host);

		return host;
	}

	
	public void resetSessionValues() {

		if (EntappiaGenieApplication.isPrimaryServer)
		{
			preference.setLongValue("primary_" + AppConstants.SESSION_TIME, 0);
			preference.setStringValue("primary_" + AppConstants.SESSION_KEY, "");
		}
		else
		{
			preference.setLongValue("secondary_" + AppConstants.SESSION_TIME, 0);
			preference.setStringValue("secondary_" + AppConstants.SESSION_KEY, "");
		}

	}
	
	public void setSessionTime() {

		if (EntappiaGenieApplication.isPrimaryServer)
			preference.setLongValue("primary_" + AppConstants.SESSION_TIME, System.currentTimeMillis());
		else
			preference.setLongValue("secondary_" + AppConstants.SESSION_TIME, System.currentTimeMillis());

	}
	
	public long getSessionTime() {

		if (EntappiaGenieApplication.isPrimaryServer)
			return preference.getLongValue("primary_" + AppConstants.SESSION_TIME);
		else
			return preference.getLongValue("secondary_" + AppConstants.SESSION_TIME);
 
	}
	
	public void saveSessionValue(String sessionId) {

		if (EntappiaGenieApplication.isPrimaryServer)
			preference.setStringValue("primary_" + AppConstants.SESSION_KEY, sessionId);
		else
			preference.setStringValue("secondary_" + AppConstants.SESSION_KEY, sessionId);

	}
	
	public String getSessionValue() {

		if (EntappiaGenieApplication.isPrimaryServer)
			return preference.getStringValue("primary_" + AppConstants.SESSION_KEY);
		else
			return preference.getStringValue("secondary_" + AppConstants.SESSION_KEY);
 
	}
}
