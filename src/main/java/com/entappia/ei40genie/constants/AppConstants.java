package com.entappia.ei40genie.constants;

public class AppConstants {

	//added
	public static final String timeZone = "Asia/Kolkata";
	
	public static enum ZoneType {
		Trackingzone, DeadZone
	}
	//previous
	public static boolean sendLogs = true;
	public static boolean print_log = true;

	public static String SESSION_KEY = "app_session_key";
	public static String LICENSE_KEY = "licenseKey";
	public static String ORGANIZATION_NAME = "org_name";
 
	public static String errormsg = "Error in process";

	public static int CURRENT_MINS = 0;

	public static String EI40_TAGS = "/ei40tag";
	public static String EI40_HELLO_API = "/ei40HelloApi";
	public static String EI40_SESSION = "/ei40CreateSession/";
	public static String EI40_CHECK_SESSION = "/checkSession/";
	public static String EI40_DOWNLOAD_API = "/ei40download";
	public static String EI40_UPDATE_LIVE_TAGS = "/updateEI40LiveTags";
	public static String EI40_MAP_DETAILS = "/ei40MapDetails";
	public static String EI40_ZONE_DETAILS = "/ei40Zone";
	public static String EI40_TAG_COLOR_DETAILS = "/updateEI40ConfigurationTagColor";
	public static String EI40_UPDATE_DEVICE_DETAILS = "/updateEI40QuuppaDeviceDetails";
	public static String EI40_TAG_POSITIONS = "/ei40TagPositions";
	public static String EI40_GET_CONFIGURATION = "/ei40Configuration";
	
	/*public static String ENTAPPIA_TAGS = "/quuppatags";
	public static String ENTAPPIA_DATA = "/quuppadata";
	public static String ENTAPPIA_ZONE = "/getQuuppaZone";
	public static String ENTAPPIA_SESSION = "/createSession/";
	public static String ENTAPPIA_CHECK_SESSION = "/checkSession/";
	public static String ENTAPPIA_UPDATE_TAG_POSITION = "/updateTagPosition";
	public static String ENTAPPIA_LOCATOR_STATUS = "/updateLocatorStatus";
	public static String ENTAPPIA_UPDATE_DEVICE_DETAILS = "/updateQuuppaDeviceDetails";

	public static String ENTAPPIA_HELLO_API = "/helloApi";
	public static String ENTAPPIA_PROJECT_INFO = "/projectinfo";
	public static String ENTAPPIA_DOWNLOAD_API = "/download";
	public static String ENTAPPIA_CONFIGURATION = "/getConfiguration";*/

	// Quuppa Apis
	public static String GET_QPE_INFO = "/qpe/getPEInfo?version=2";
	public static String GET_PROJECT_INFO = "/qpe/getProjectInfo?version=2";
	public static String GET_TAG_URL = "/qpe/getTagData?version=2&maxAge=60000&radius=10";
	public static String GET_LOCATOR_INFO = "/qpe/getLocatorInfo?version=2";

	
	public static String ERROR_CONECTION_TYPE_ENTAPPIA = "Entappia Connection";
	public static String ERROR_CONECTION_TYPE_QPE = "QPE Connection";
	public static String ERROR_CONECTION_TYPE_GENIE_PROFILE = "Genie Connection";
	//public static String ERROR_CONECTION_TYPE_CT_PROFILE = "CTP Connection";
	public static String ERROR_CONECTION_TYPE_LOCATOR_STATUS = "Locator Connection";

	// Configs Constants 

	public static String CONFIG_PSN = "Primary Server";
	public static String CONFIG_SSN = "Secondary Server";
	public static String CONFIG_TTA = "Transaction Table Age";
	public static String CONFIG_LTA = "Logger Table Age";
	public static String CONFIG_HA = "Hello Api";
	public static String CONFIG_LTT = "Live Tag Tracing";
	public static String CONFIG_LS = "Locator Status";
	public static String CONFIG_DUR = "Data Update Rate";
	public static String CONFIG_ARC = "Alert Repeat Count";
	public static String CONFIG_ARI = "Alert Repeat Interval";
	
	public static String CONFIG_STF = "Staff data from Quuppa";
	public static String CONFIG_AST = "Asset data from Quuppa";
	public static String CONFIG_VIS = "Visitor data from Quuppa";
	public static String CONFIG_MAT = "Material data from Quuppa";
	
	
	// Configs PREFERENCE Key Constants 
	public static String CONFIG_PREF_TTA = "TransactionTableAge";
	public static String CONFIG_PREF_LTA = "LoggerTableAge";
	public static String CONFIG_PREF_HA = "helloApi";
	public static String CONFIG_PREF_LTT = "liveTagTracing";
	public static String CONFIG_PREF_LS = "locatorStatus";
	public static String CONFIG_PREF_DUR = "dataUpdateRate";
	public static String CONFIG_PREF_ARC = "alertRepeatCount";
	public static String CONFIG_PREF_ARI = "alertRepeatInterval";

	public static String CONFIG_PREF_STF = "staffDataFromQuuppa";
	public static String CONFIG_PREF_AST = "assetDataFromQuuppa";
	public static String CONFIG_PREF_VIS = "visitorDataFromQuuppa";
	public static String CONFIG_PREF_MAT = "materialDataFromQuuppa";
	
	
	public static String CONFIG_LMT = "configLastModifiedTime";
	public static String SESSION_TIME = "session_time";

	public static String PRIMARY_SERVER_NAME = "primary_host_name";
	public static String SECONDARY_SERVER_NAME = "secondary_host_name";
	
	
	public static String EI40_LOCAL_SERVER_PATH = "http://localhost:8080/EI40/";
	public static String EI40_LOCAL_SERVER_LOGIN_API = "login";
	
	public static String EI40_PEOPLE_DWELL_TIME_API = "peopleDwellTime";
	public static String EI40_VISITOR_DWELL_TIME_API = "visitorDwellTime";
	public static String EI40_ASSET_DWELL_TIME_API = "assetDwellTime";
	public static String EI40_WORK_STATION_FLOW_API = "WorkStationFlowAnalytics";
	public static String EI40_WORK_STATION_CONSTRAINT_API = "WorkStationConstraintAnalytics";
	public static String EI40_WORK_STATION_AGING_API = "WorkStationAgingAnalytics";
	public static String EI40_WORK_STATION_INVENTORY_API = "WorkStationInventory";
	public static String EI40_WORK_ORDER_TIME_API = "WorkOrderTimeAnalytics";
	public static String EI40_ZONE_FOOTFALL_API = "ZoneFootFall";
	public static String EI40_ZONE_ATTENDANCE_API = "attendance";
	
	
	
	
	
	
	
	

}
