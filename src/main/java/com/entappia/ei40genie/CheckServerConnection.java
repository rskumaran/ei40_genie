package com.entappia.ei40genie;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.entappia.EntappiaApiService;
import com.entappia.ei40genie.utils.Logger;

@Component
public class CheckServerConnection {

	@Autowired
	Logger logger;
	
	@Autowired
	private Preference preference;

	@Autowired
	private EntappiaApiService entappiaApiService;
	@Autowired
	private EntappiaGenieApplication entappiaGenieApplication;

	public static String connectedServer = "";
	public static int primaryServerErrorCount = 0;
	public static boolean addPrimaryServerLog = true;
	public static boolean addSecondaryServerLog = true;
	
	
	/*@Scheduled(cron = "0 0/1 * * * *")
	public void CheckConnection() throws ExecutionException {

		if (AppConstants.print_log)
			System.out.println(" CheckConnection" + Utils.getFormatedDate2(new Date()));
		
		
		if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY))) {
			
			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {

					if (AppConstants.print_log)
						System.out.println(" ServerConnected = " + EntappiaGenieApplication.isServerConnected + 
								"IsPrimaryserver =  "+ EntappiaGenieApplication.isPrimaryServer + "time = " + Utils.getFormatedDate2(new Date()));
					if (!EntappiaGenieApplication.isServerConnected || !EntappiaGenieApplication.isPrimaryServer) {
						if (AppConstants.print_log)
							System.out.println("LICENSE_KEY Got form QPE " + Utils.getFormatedDate2(new Date()));

						CompletableFuture<Boolean> primaryCompletableFuture;
						try {

							if (addPrimaryServerLog)
								addLogs("Event", "Genie", "Server Connection Checking",
										"Checking primary server connection("
												+ preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME) + ").");

							addPrimaryServerLog = false;

							primaryCompletableFuture = entappiaApiService
									.isServerConncted(preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME));

							CompletableFuture.allOf(primaryCompletableFuture).join();

							if (primaryCompletableFuture.isDone()) {

								boolean isPrimaryServerOn;
								try {
									isPrimaryServerOn = primaryCompletableFuture.get();

									if (isPrimaryServerOn) {
										primaryServerErrorCount = 0;
										EntappiaGenieApplication.isPrimaryServer = true;
										if (AppConstants.print_log)
											System.out.println("Primary Server is On");
										entappiaGenieApplication.createEntappiaSession(
												preference.getStringValue(AppConstants.LICENSE_KEY));
										return;
									} else {
										if (primaryServerErrorCount <= 3)
											primaryServerErrorCount++;

										if (AppConstants.print_log)
											System.out.println("Primary Server is Off");
									}

								} catch (ExecutionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						addPrimaryServerLog = true;
					}

					if (AppConstants.print_log)
						System.out.println(" ServerConnected = " + EntappiaGenieApplication.isServerConnected + 
								"IsPrimaryserver =  "+ EntappiaGenieApplication.isPrimaryServer + "Primary server error count = " 
								+ primaryServerErrorCount + "time = " + Utils.getFormatedDate2(new Date()));
					
					if (primaryServerErrorCount >= 3) {

						if (!EntappiaGenieApplication.isServerConnected) {

							EntappiaGenieApplication.isPrimaryServer = false;

							if (addSecondaryServerLog)
								addLogs("Event", "Genie", "Server Connection Checking",
										"Checking secondary server connection("
												+ preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME) + ").");

							addSecondaryServerLog = false;

							CompletableFuture<Boolean> secondaryCompletableFuture;
							try {
								secondaryCompletableFuture = entappiaApiService.isServerConncted(
										preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME));

								CompletableFuture.allOf(secondaryCompletableFuture).join();

								if (secondaryCompletableFuture.isDone()) {

									boolean isSecondaryServerOn;
									try {
										isSecondaryServerOn = secondaryCompletableFuture.get();

										if (isSecondaryServerOn) {

											if (AppConstants.print_log)
												System.out.println("Secondary Server is On");
											if (AppConstants.print_log)
												System.out.println("Called create session");
											entappiaGenieApplication.createEntappiaSession(
													preference.getStringValue(AppConstants.LICENSE_KEY));
											return;
										} else {
											if (AppConstants.print_log)
												System.out.println("Secondary Server is Off");
										}
									} catch (ExecutionException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					} else {
						addSecondaryServerLog = true;
					}

				}
			});
			executorService.shutdown();

		} else {
			primaryServerErrorCount = 0;
			if (AppConstants.print_log)
				System.out.println("NO LICENSE_KEY " + Utils.getFormatedDate2(new Date()));
		}
	}*/

	@Scheduled(cron = "0 0/1 * * * *")
	public void sendTagPositions() throws ExecutionException {

		if (AppConstants.print_log)
			System.out.println(" CheckServerConnection sendTagPositions " + Utils.getFormatedDate2(new Date()));
		
		
		if (!Utils.isEmptyString(preference.getStringValue(AppConstants.LICENSE_KEY))) {
			
			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {

					if (AppConstants.print_log)
						System.out.println(" ServerConnected = " + EntappiaGenieApplication.isServerConnected + 
								"IsPrimaryserver =  "+ EntappiaGenieApplication.isPrimaryServer + "time = " + Utils.getFormatedDate2(new Date()));
					if (!EntappiaGenieApplication.isServerConnected || !EntappiaGenieApplication.isPrimaryServer) {
						if (AppConstants.print_log)
							System.out.println("LICENSE_KEY Got form QPE " + Utils.getFormatedDate2(new Date()));

						CompletableFuture<Boolean> primaryCompletableFuture;
						try {

							if (addPrimaryServerLog)
								addLogs("Event", "Genie", "Server Connection Checking",
										"Checking primary server connection("
												+ preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME) + ").");

							addPrimaryServerLog = false;

							primaryCompletableFuture = entappiaApiService
									.isServerConncted(preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME));

							CompletableFuture.allOf(primaryCompletableFuture).join();

							if (primaryCompletableFuture.isDone()) {

								boolean isPrimaryServerOn;
								try {
									isPrimaryServerOn = primaryCompletableFuture.get();

									if (isPrimaryServerOn) {
										primaryServerErrorCount = 0;
										EntappiaGenieApplication.isPrimaryServer = true;
										if (AppConstants.print_log)
											System.out.println("Primary Server is On");
										entappiaGenieApplication.createEntappiaSession(
												preference.getStringValue(AppConstants.LICENSE_KEY));
										return;
									} else {
										if (primaryServerErrorCount <= 3)
											primaryServerErrorCount++;

										if (AppConstants.print_log)
											System.out.println("Primary Server is Off");
									}

								} catch (ExecutionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						addPrimaryServerLog = true;
					}

					if (AppConstants.print_log)
						System.out.println(" ServerConnected = " + EntappiaGenieApplication.isServerConnected + 
								"IsPrimaryserver =  "+ EntappiaGenieApplication.isPrimaryServer + "Primary server error count = " 
								+ primaryServerErrorCount + "time = " + Utils.getFormatedDate2(new Date()));
					
					if (primaryServerErrorCount >= 3) {

						if (!EntappiaGenieApplication.isServerConnected) {

							EntappiaGenieApplication.isPrimaryServer = false;

							if (addSecondaryServerLog)
								addLogs("Event", "Genie", "Server Connection Checking",
										"Checking secondary server connection("
												+ preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME) + ").");

							addSecondaryServerLog = false;

							CompletableFuture<Boolean> secondaryCompletableFuture;
							try {
								secondaryCompletableFuture = entappiaApiService.isServerConncted(
										preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME));

								CompletableFuture.allOf(secondaryCompletableFuture).join();

								if (secondaryCompletableFuture.isDone()) {

									boolean isSecondaryServerOn;
									try {
										isSecondaryServerOn = secondaryCompletableFuture.get();

										if (isSecondaryServerOn) {

											if (AppConstants.print_log)
												System.out.println("Secondary Server is On");
											if (AppConstants.print_log)
												System.out.println("Called create session");
											entappiaGenieApplication.createEntappiaSession(
													preference.getStringValue(AppConstants.LICENSE_KEY));
											return;
										} else {
											if (AppConstants.print_log)
												System.out.println("Secondary Server is Off");
										}
									} catch (ExecutionException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					} else {
						addSecondaryServerLog = true;
					}

				}
			});
			executorService.shutdown();

		} else {
			primaryServerErrorCount = 0;
			if (AppConstants.print_log)
				System.out.println("NO LICENSE_KEY " + Utils.getFormatedDate2(new Date()));
		}
	}

	public void addLogs(String type, String major, String minor, String message) {
		
		logger.addLogs(type, major, minor, message);
	}
}
