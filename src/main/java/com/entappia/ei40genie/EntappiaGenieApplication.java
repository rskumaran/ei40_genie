package com.entappia.ei40genie;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.constants.AppConstants.ZoneType;
import com.entappia.ei40genie.dbmodels.CampusDetails;
import com.entappia.ei40genie.dbmodels.Configuration;
import com.entappia.ei40genie.dbmodels.Doors;
import com.entappia.ei40genie.dbmodels.FirmwareSettings;
import com.entappia.ei40genie.dbmodels.OutlinesDetails;
import com.entappia.ei40genie.dbmodels.Partitions;
import com.entappia.ei40genie.dbmodels.PathWay;
import com.entappia.ei40genie.dbmodels.Tags;
import com.entappia.ei40genie.dbmodels.Walls;
import com.entappia.ei40genie.dbmodels.ZoneClassification;
import com.entappia.ei40genie.dbmodels.ZoneDetails;
import com.entappia.ei40genie.entappia.EntappiaApiService;
import com.entappia.ei40genie.models.ApplicationConfigurationSettings;
import com.entappia.ei40genie.quuppa.QuuppaApiService;
import com.entappia.ei40genie.repository.CampusDetailsRepository;
import com.entappia.ei40genie.repository.ConfigurationRepository;
import com.entappia.ei40genie.repository.DoorsRepository;
import com.entappia.ei40genie.repository.FirmwareSettingsRepository;
import com.entappia.ei40genie.repository.OutLineRepository;
import com.entappia.ei40genie.repository.PartitionsRepository;
import com.entappia.ei40genie.repository.PathWayRepository;
import com.entappia.ei40genie.repository.ProfileConfigurationRepository;
import com.entappia.ei40genie.repository.TagsRepository;
import com.entappia.ei40genie.repository.WallsRepository;
import com.entappia.ei40genie.repository.ZoneClassificationRepository;
import com.entappia.ei40genie.repository.ZoneRepository;
import com.entappia.ei40genie.utils.HelloApiTask;
import com.entappia.ei40genie.utils.LocatorStatusTask;
import com.entappia.ei40genie.utils.Logger;
import com.entappia.ei40genie.utils.SendDeviceDetails;
import com.entappia.ei40genie.utils.SendTagPostionTask;
import com.entappia.ei40genie.utils.TagBackgroundTasks;
import com.entappia.ei40genie.utils.TagSchedulerTask;

@SpringBootApplication
//@EnableEurekaServer
@EnableScheduling
@EnableAsync
public class EntappiaGenieApplication {

	@Autowired
	Logger logger;

	@Autowired
	private SendTagPostionTask sendingTagPostionTask;

	@Autowired
	private TagSchedulerTask tagSchedulerTask;

	@Autowired
	private LocatorStatusTask locatorStatusTask;

	@Autowired
	private TagBackgroundTasks tagBackgroundTasks;

	private TagsRepository tagRepository;

	@Autowired
	private HelloApiTask helloApiTask;

	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	private EntappiaApiService entappiaApiService;

	@Autowired
	private ProfileConfigurationRepository profileConfigurationRepository;

	@Autowired
	private FirmwareSettingsRepository firmwareSettingsRepository;

	@Autowired
	private SendDeviceDetails sendDeviceDetailsTask;

	@Autowired
	private ApplicationConfigurationSettings applicationConfigurationSettings;

	@Autowired 
	private ConfigurationRepository configurationRepository;

	@Autowired 
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired 
	private OutLineRepository outLineRepository;

	@Autowired 
	private PartitionsRepository partitionsRepository;

	@Autowired 
	private PathWayRepository pathWayRepository;

	@Autowired 
	private DoorsRepository doorsRepository;
	
	@Autowired 
	private WallsRepository wallsRepository;
	
	@Autowired
	private ZoneRepository zoneRepository;
	
	@Autowired
	private ZoneClassificationRepository zoneClassificationRepository;

	static Preference preference = new Preference();

	public static double xMeter = 0;
	public static double yMeter = 0;
	public static double heightMeter = 0;
	public static double metersPerPixelX = 0;
	public static double metersPerPixelY = 0;

	public static boolean isServerConnected = false;

	public static boolean isPrimaryServer = true;

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		SpringApplication.run(EntappiaGenieApplication.class, args);
	}

	@Bean(name = "asyncExecutor")
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(3);
		executor.setMaxPoolSize(3);
		executor.setQueueCapacity(100);
		executor.setThreadNamePrefix("AsynchThread-");
		executor.initialize();
		return executor;
	}

	@PostConstruct
	void getQPEInfo() {

		try {
			// getZoneData();
			ResourceBundle bundle = ResourceBundle.getBundle("application");

			FirmwareSettings firmwareSettings = firmwareSettingsRepository.findByApplication("genie");
			
			if (firmwareSettings == null) {
				firmwareSettings = new FirmwareSettings();
				firmwareSettings.setApplication("genie");
				firmwareSettings.setDownloadedVersion("");
				firmwareSettings.setCreatedDate(new Date());
				firmwareSettings.setModifiedDate(new Date());
				firmwareSettings.setVersion(bundle.getString("genieversion"));
			}else {

				String versionInDB = firmwareSettings.getVersion();

				if(versionInDB == null || !versionInDB.equals(bundle.getString("genieversion"))) {

					firmwareSettings.setVersion(bundle.getString("genieversion"));
					firmwareSettings.setCreatedDate(new Date());
					firmwareSettings.setModifiedDate(new Date());
				}
				if( firmwareSettings.getDownloadedVersion() == null || firmwareSettings.getDownloadedVersion().isEmpty()) {
					firmwareSettings.setDownloadedVersion("");
				}
				if( firmwareSettings.getCreatedDate() == null || firmwareSettings.getCreatedDate().toString().isEmpty()) {
					firmwareSettings.setCreatedDate(new Date());
				}

				if( firmwareSettings.getModifiedDate() == null || firmwareSettings.getModifiedDate().toString().isEmpty()) {
					firmwareSettings.setModifiedDate(new Date());
				}


			}
			
			
			firmwareSettingsRepository.save(firmwareSettings);

			/*
			 * Settings ctFW = settingsRepository.findByApplication("ctprofile"); if (ctFW
			 * == null) { ctFW = new Settings(); ctFW.setApplication("ctprofile"); }
			 * 
			 * ctFW.setVersion(bundle.getString("ctversion"));
			 * settingsRepository.save(ctFW);
			 */


			entappiaApiService.resetSessionValues();

			preference.setStringValue(AppConstants.PRIMARY_SERVER_NAME, bundle.getString("DEFAULT_HOST_NAME"));
			preference.setStringValue(AppConstants.SECONDARY_SERVER_NAME, bundle.getString("DEFAULT_SECONDARY_NAME"));

			preference.setStringValue("licenseKey", "");
			if (AppConstants.print_log)
				System.out.println("getQPEStart 1");
			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {
					if (AppConstants.print_log)
						System.out.println("getQPEStart");
					try {
						CompletableFuture<JSONObject> qpeCompletableFuture = quuppaApiService.getQPEInfo();
						CompletableFuture.allOf(qpeCompletableFuture).join();

						if (qpeCompletableFuture.isDone()) {

							try {
								JSONObject jsonObject = qpeCompletableFuture.get();
								if (jsonObject != null) {
									String status = jsonObject.optString("status");
									if (AppConstants.print_log)
										System.out.println("getQPEStart status==" + status);

									if (!Utils.isEmptyString(status) && status.equals("success")) {
										if (AppConstants.print_log)
											System.out.println("getQPEStart success==" + status);

										String licenseKey = jsonObject.getString("licenseKey");

										if (!Utils.isEmptyString(licenseKey)) {
											preference.setStringValue(AppConstants.LICENSE_KEY, licenseKey);

											/*
											 * if (checkServerConnection()) { addLogs("Event", "Genie",
											 * "Server Connection Checking", "Primary Server is active");
											 * isPrimaryServer = true; } else isPrimaryServer = false;
											 */

										}

										String qpeVersion = jsonObject.getString("qpeVersion");
										if (!Utils.isEmptyString(qpeVersion)) {
											FirmwareSettings firmwareSettings = firmwareSettingsRepository.findByApplication("qpe");
											
											if (firmwareSettings == null) {
												firmwareSettings = new FirmwareSettings();
												firmwareSettings.setApplication("qpe");
												firmwareSettings.setDownloadedVersion("");
												firmwareSettings.setCreatedDate(new Date());
												firmwareSettings.setModifiedDate(new Date());
												firmwareSettings.setVersion(qpeVersion.split("-")[0]);
											}else {

												String versionInDB = firmwareSettings.getVersion();

												if(versionInDB == null || !versionInDB.equals(qpeVersion.split("-")[0])) {

													firmwareSettings.setVersion(qpeVersion.split("-")[0]);
													firmwareSettings.setCreatedDate(new Date());
													firmwareSettings.setModifiedDate(new Date());
												}
												if( firmwareSettings.getDownloadedVersion() == null || firmwareSettings.getDownloadedVersion().isEmpty()) {
													firmwareSettings.setDownloadedVersion("");
												}
												if( firmwareSettings.getCreatedDate() == null || firmwareSettings.getCreatedDate().toString().isEmpty()) {
													firmwareSettings.setCreatedDate(new Date());
												}

												if( firmwareSettings.getModifiedDate() == null || firmwareSettings.getModifiedDate().toString().isEmpty()) {
													firmwareSettings.setModifiedDate(new Date());
												}


											}
																						
											firmwareSettingsRepository.save(firmwareSettings);
								
										}

										//alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_QPE);
										addLogs("Event", "Genie", "Get QPE Info", "Success");

									} else {
										if (AppConstants.print_log)
											System.out.println("getQPEStart error==" + status);

										getQPEData();

										String message = jsonObject.optString("message");
										addLogs("Error", "Genie", "Get QPE Info", message);
										//alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_QPE, message);
									}
								}
							} catch (ExecutionException e) {
								addLogs("Error", "Genie", "Get QPE Info", e.getMessage());
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					} catch (InterruptedException e) {
						addLogs("Error", "Genie", "Get QPE Info", e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			executorService.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getQPEData() {
		if (AppConstants.print_log)
			System.out.println("getQPEData");
		if (AppConstants.print_log)
			System.out.println(Utils.getFormatedDate(new Date()));

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 1);

		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
		threadPoolTaskScheduler.initialize();
		threadPoolTaskScheduler.schedule(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (AppConstants.print_log)
					System.out.println(Utils.getFormatedDate(new Date()));
				getQPEInfo();
			}
		}, calendar.getTime());
	}


	long previousSessionDataRunTime = 0, previousCreateSessionRunTime = 0;

	public void getSessionData() {

		Calendar calendar1 = Calendar.getInstance();
		calendar1.add(Calendar.MINUTE, -1);

		if (previousSessionDataRunTime < calendar1.getTimeInMillis()) {

			stopScheduleTasks();
			previousSessionDataRunTime = System.currentTimeMillis();

		}
	}

	public boolean checkEntappiaSession() {

		try {

			addLogs("Event", "Genie", "Server Connection Checking", "checking Entappia Session");
			CompletableFuture<JSONObject> sessionCompletableFuture = entappiaApiService.checkSession();
			CompletableFuture.allOf(sessionCompletableFuture).join();

			JSONObject sessionInfoJsonObject = null;
			if (sessionCompletableFuture.isDone()) {
				sessionInfoJsonObject = sessionCompletableFuture.get();
				if (sessionInfoJsonObject != null) {
					String status = sessionInfoJsonObject.optString("status");
					if (!Utils.isEmptyString(status) && status.equals("success")) {
						return true;
					}
				}
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}



	public void createEntappiaSession(String licenseKey) {

		System.out.println("createEntappiaSession" + Utils.getFormatedDate2(new Date()));
		Calendar calendar1 = Calendar.getInstance();
		calendar1.add(Calendar.MINUTE, -1);

		if (previousCreateSessionRunTime < calendar1.getTimeInMillis()) {

			previousCreateSessionRunTime = System.currentTimeMillis();

			/*
			 * if (!isPrimaryServer) { if (AppConstants.print_log)
			 * System.out.println("startPrimaryServerChecking");
			 * checkPrimaryServerTask.createSchedule(); }
			 */

			stopScheduleTasks();
			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {

					long pre_session_time = preference.getLongValue(AppConstants.SESSION_TIME);
					Calendar cal = Calendar.getInstance();

					long diff = cal.getTimeInMillis() - pre_session_time;

					if (pre_session_time > 0 && !Utils.isEmptyString(entappiaApiService.getSessionValue())
							&& diff < 1800000)// 30 mins
					{

						if (checkEntappiaSession()) {

							if (isPrimaryServer)
								addLogs("Event", "Genie", "Server Connection Checking",
										"Server Connected with primary server("
												+ preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME) + ").");
							else
								addLogs("Event", "Genie", "Server Connection Checking",
										"Server Connected with Secondary server("
												+ preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME) + ").");

							isServerConnected = true;
							startScheduleTasks();
						} else
							createSession(licenseKey);
					} else {
						createSession(licenseKey);
					}

				}
			});
			executorService.shutdown();
		}
	}

	private void createSession(String licenseKey) {

		if (AppConstants.print_log)
			System.out.println("createSession");

		CompletableFuture<JSONObject> sessionCompletableFuture;
		try {
			sessionCompletableFuture = entappiaApiService.createSession(licenseKey);
			//check here for not null
			CompletableFuture.allOf(sessionCompletableFuture).join();

			JSONObject sessionInfoJsonObject = null;
			if (sessionCompletableFuture.isDone()) {
				try {
					sessionInfoJsonObject = sessionCompletableFuture.get();
					// isSessionCalled = false;

					if (sessionInfoJsonObject != null) {
						String status = sessionInfoJsonObject.optString("status");

						if (!Utils.isEmptyString(status) && status.equals("success")) {
							String sessionId = sessionInfoJsonObject.optString("sessionId");
							String organization = sessionInfoJsonObject.optString("organization");
							if (!Utils.isEmptyString(sessionId)) {

								entappiaApiService.saveSessionValue(sessionId);
								preference.setStringValue(AppConstants.ORGANIZATION_NAME, organization);
								
								isServerConnected = true;
								 startScheduleTasks();
								//startTagScheduler();
								sendTagData();
								updateConfigurationData();
								sendProjectData();
								sendZoneData();
								getProfileConfiguration();
								String server = "";
								if (isPrimaryServer)
									server = " Primary Server ("
											+ preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME) + ").";
								else
									server = " Secondary Server ("
											+ preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME) + ").";

								addLogs("Event", "Genie", "Server Connection Checking",
										"New Session created with " + server);

							} else {
								addLogs("Error", "Genie", "Create Entappia Session", "Session key is empty.");
							}
						} else {
							entappiaApiService.resetSessionValues();
							String message = sessionInfoJsonObject.optString("message");

							getSessionData();
							addLogs("Error", "Genie", "Create Entappia Session", message);
						}
					}
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					addLogs("Error", "Genie", "Create Entappia Session", e.getMessage());
				}

			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			addLogs("Error", "Genie", "Create Entappia Session", e.getMessage());
		}

	}

	
	private void updateConfigurationData()
	{
		try {

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {

					try {

						HashMap<String, Object> projectData =  new HashMap<>();
						projectData = frameConfigurationData();
						entappiaApiService.postServerCall(projectData, AppConstants.EI40_TAG_COLOR_DETAILS);

					} catch (InterruptedException e) {
						
						e.printStackTrace();
					}
					catch (JSONException e) {
						addLogs("Error", "Genie", "Send Zone Info-EntappiaGenieApp", e.getMessage());
						e.printStackTrace();
					}
				}
			});
			executorService.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private HashMap<String, Object> frameConfigurationData()
	{


		HashMap<String, Object> tagColorDetails = new HashMap<>();
		List<Configuration> configurationDetails = new ArrayList<>();
		configurationDetails = configurationRepository.findAll();
		for(int i=0;i < configurationDetails.size();i++)
		{
			Configuration configuration = configurationDetails.get(i);
			tagColorDetails.put("Warehouse",configuration.getWareHouseTagColor());
			tagColorDetails.put("Visitor",configuration.getVisitorTagColor());
			tagColorDetails.put( "RMA", configuration.getRmaTagColor());
			tagColorDetails.put("Staff",configuration.getStaffTagColor());
			tagColorDetails.put("Material", configuration.getMaterialTagColor());
			tagColorDetails.put("Vendor", configuration.getVendorTagColor());
			tagColorDetails.put("LSLM", configuration.getLSLMTagColor());
			tagColorDetails.put("Vehicle", configuration.getVehicleTagColor());
			tagColorDetails.put("Contractor",configuration.getContractorTagColor());
			tagColorDetails.put("Parts", configuration.getPartTagColor());
			tagColorDetails.put("Gage", configuration.getGageTagColor());
			tagColorDetails.put( "Fixture", configuration.getFixtureTagColor());
			tagColorDetails.put("Supplies",configuration.getSuppliesTagColor());
			tagColorDetails.put("Inventory", configuration.getInventoryTagColor());
			tagColorDetails.put("Tool", configuration.getToolTagColor());
		}
		
		return tagColorDetails;
	
	}
	
	private void sendZoneData()
	{
		try {

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {

					try {

						HashMap<String, Object> projectData =  new HashMap<>();
						projectData = frameZoneData();
						entappiaApiService.postServerCall(projectData, AppConstants.EI40_ZONE_DETAILS);

					} catch (InterruptedException e) {
						
						e.printStackTrace();
					}
					catch (JSONException e) {
						addLogs("Error", "Genie", "Send Zone Info-EntappiaGenieApp", e.getMessage());
						e.printStackTrace();
					}
				}
			});
			executorService.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected HashMap<String, Object> frameZoneData() {
		HashMap<String, Object> zoneData =  new HashMap<>();
		
		List<ZoneClassification> zoneClassificationList = new ArrayList<>();
		zoneClassificationList = zoneClassificationRepository.findAll();
		
		HashMap<String, Object> zoneClassName = new HashMap<>();
		for(int i=0;i < zoneClassificationList.size();i++)
		{
			ZoneClassification zoneClassification = zoneClassificationList.get(i); 
			if(zoneClassification.isActive() == true) {
				zoneClassName.put(zoneClassification.getClassName(), zoneClassification.getColor());
			}
		}
		
		zoneData.put("classifications", zoneClassName);
		
		 
		List<ZoneDetails> zoneDetailsList = new ArrayList<>();
		zoneDetailsList = zoneRepository.findAll();
		
		HashMap<String, Object> allZoneDetailsMap = new HashMap<>();
		
		for(int i=0;i < zoneDetailsList.size();i++)
		{
			
			ZoneDetails zoneDetails = zoneDetailsList.get(i);

			if(zoneDetails.isActive() == true) {
				HashMap<String, Object> ZoneDetailsMap = new HashMap<>();
				  
				ZoneDetailsMap.put("id", String.valueOf(zoneDetails.getId()));
				ZoneDetailsMap.put("name", zoneDetails.getName());
				ZoneDetailsMap.put("accessControl", zoneDetails.getAccessControl());
				ZoneDetailsMap.put("area", zoneDetails.getArea());
				ZoneDetailsMap.put("capacity", zoneDetails.getCapacity());
				ZoneDetailsMap.put("coordinateList", zoneDetails.getCoordinateList()); 
				ZoneDetailsMap.put("userType", zoneDetails.getType());
				ZoneDetailsMap.put("zoneType", zoneDetails.getZoneType()==ZoneType.Trackingzone?"Tracking Zone":"Dead Zone");
				ZoneDetailsMap.put("classificationName", zoneDetails.getZoneClassification().getClassName());
				ZoneDetailsMap.put("geometryType", zoneDetails.getGeometryType());
				ZoneDetailsMap.put("descriptions", zoneDetails.getDescription()); 
				 
				allZoneDetailsMap.put(String.valueOf(zoneDetails.getId()), ZoneDetailsMap );
			}
		}
		
		zoneData.put("zones",allZoneDetailsMap);

		return zoneData;
	}
	// @PostConstruct
	void sendProjectData() {

		try {

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {

					try {

						HashMap<String, Object> projectData =  new HashMap<>();
						projectData = frameProjectData();
						entappiaApiService.postServerCall(projectData, AppConstants.EI40_MAP_DETAILS);

					} catch (InterruptedException e) {
						
						e.printStackTrace();
					}
					catch (JSONException e) {
						addLogs("Error", "Genie", "Get Project Info-EntappiaGenieApp", e.getMessage());
						e.printStackTrace();
					}
				}
			});
			executorService.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	protected HashMap<String, Object> frameProjectData() {

		HashMap<String, Object> projectData =  new HashMap<>();

		List<Configuration> configurationDetails = new ArrayList<>();
		configurationDetails = configurationRepository.findAll();
		for(int i=0;i < configurationDetails.size();i++)
		{
			Configuration configuration = configurationDetails.get(i);
			projectData.put("organization",  configuration.getOrgName());
		}

		List<CampusDetails> campusDetailsList = new ArrayList<>();
		campusDetailsList = campusDetailsRepository.findAll();

		for(int i=0;i < campusDetailsList.size();i++)
		{

			CampusDetails campusDetails = campusDetailsList.get(i);

			if(campusDetails.isActive() == true) {
				HashMap<String, Object> campusDetailsMap = new HashMap<>();

				campusDetailsMap.put("name", campusDetails.getName());
				campusDetailsMap.put("colorCode", campusDetails.getColorCode());
				campusDetailsMap.put("height", campusDetails.getHeight());
				campusDetailsMap.put("width", campusDetails.getWidth());
				campusDetailsMap.put("ppmX", campusDetails.getMetersPerPixelX());
				campusDetailsMap.put("ppmY", campusDetails.getMetersPerPixelY());
				campusDetailsMap.put("originX", campusDetails.getOriginX());
				campusDetailsMap.put("originY", campusDetails.getOriginY());


				projectData.put("ei40MapData", campusDetailsMap );
			}
		}


		List<OutlinesDetails> outlinesDetailsList = new ArrayList<>();
		outlinesDetailsList = outLineRepository.findAll();

		HashMap<String, Object> allOutlinesDetailsMap = new HashMap<>();

		for(int i=0;i < outlinesDetailsList.size();i++)
		{

			OutlinesDetails outlinesDetails = outlinesDetailsList.get(i);


			if(outlinesDetails.isActive() == true) {
				HashMap<String, Object> outlinesDetailsMap = new HashMap<>();

				outlinesDetailsMap.put("name", outlinesDetails.getName());
				outlinesDetailsMap.put("outlinesId", outlinesDetails.getOutlinesId());
				outlinesDetailsMap.put("bgColor", outlinesDetails.getBgColor());
				outlinesDetailsMap.put("coordinateList", outlinesDetails.getCoordinateList());
				outlinesDetailsMap.put("geometryType", outlinesDetails.getGeometryType());
				outlinesDetailsMap.put("outlinesArea", outlinesDetails.getOutlinesArea());
				outlinesDetailsMap.put("placeType", outlinesDetails.getPlaceType());
				outlinesDetailsMap.put("wallType", outlinesDetails.getWallType());

				allOutlinesDetailsMap.put(String.valueOf(outlinesDetails.getOutlinesId()), outlinesDetailsMap );
			}
		}
		projectData.put("outlineDetails", allOutlinesDetailsMap );


		List<Partitions> partitionsList = new ArrayList<>();
		partitionsList = partitionsRepository.findAllActivePartitions();

		HashMap<String, Object> allPartitionsMap = new HashMap<>();

		for(int i=0;i < partitionsList.size();i++)
		{

			Partitions partitions = partitionsList.get(i);


			if(partitions.isActive() == true) {
				HashMap<String, Object> partitionsMap = new HashMap<>();

				partitionsMap.put("id", partitions.getId());
				partitionsMap.put("name", partitions.getName());
				partitionsMap.put("colorCode", partitions.getColorCode());
				partitionsMap.put("coordinatesList", partitions.getCoordinates());
				partitionsMap.put("geometryType", partitions.getGeometryType());
				partitionsMap.put("wallType", partitions.getWallType());
				partitionsMap.put("outlinesId", partitions.getOutlinesDetails().getOutlinesId());

				allPartitionsMap.put(String.valueOf(partitions.getId()), partitionsMap );
			}
		}
		projectData.put("partitions", allPartitionsMap );



		List<PathWay> pathWayList = new ArrayList<>();
		pathWayList = pathWayRepository.findAll();

		HashMap<String, Object> allPathWaysMap = new HashMap<>();

		for(int i=0;i < pathWayList.size();i++)
		{

			PathWay pathWay = pathWayList.get(i);


			if(pathWay.isActive() == true) {
				HashMap<String, Object> pathWayMap = new HashMap<>();

				pathWayMap.put("id", pathWay.getId());
				pathWayMap.put("name", pathWay.getName());
				pathWayMap.put("colorCode", pathWay.getColorCode());
				pathWayMap.put("coordinatesList", pathWay.getCoordinates());
				pathWayMap.put("geometryType", pathWay.getGeometryType());
				pathWayMap.put("pathwayPattern", pathWay.getPathwayPattern());
				pathWayMap.put("pathwayType", pathWay.getPathwayType());

				allPathWaysMap.put(String.valueOf(pathWay.getId()), pathWayMap );
			}
		}
		projectData.put("pathways", allPathWaysMap );



		List<Doors> doorsList = new ArrayList<>();
		doorsList = doorsRepository.findAllActiveDoors();

		HashMap<String, Object> allDoorsMap = new HashMap<>();

		for(int i=0;i < doorsList.size();i++)
		{

			Doors doors = doorsList.get(i);


			if(doors.isActive() == true) {
				HashMap<String, Object> doorsMap = new HashMap<>();

				doorsMap.put("id", doors.getId());
				doorsMap.put("name", doors.getName());
				doorsMap.put("colorCode", doors.getColorCode());
				doorsMap.put("coordinatesList", doors.getCoordinates());
				doorsMap.put("wallType", doors.getWallType());
				doorsMap.put("outlinesId", doors.getOutlinesDetails().getOutlinesId());

				allDoorsMap.put(String.valueOf(doors.getId()), doorsMap );
			}
		}
		projectData.put("doors", allDoorsMap );
		
		
		List<Walls> wallsList  = wallsRepository.findAllActiveWalls();
		HashMap<String, Object> allWallsMap = new HashMap<>();
		
		for(int i=0;i < wallsList.size();i++)
		{

			Walls walls = wallsList.get(i);


			if(walls.isActive() == true) {
				HashMap<String, Object> wallsMap = new HashMap<>();

				wallsMap.put("id", walls.getId());
				wallsMap.put("name", walls.getName());
				wallsMap.put("colorCode", walls.getColorCode());
				wallsMap.put("coordinatesList", walls.getCoordinates());
				wallsMap.put("wallType", walls.getWallType());
				wallsMap.put("outlinesId", walls.getOutlinesDetails().getOutlinesId());
				wallsMap.put("geometryType", walls.getGeometryType());

				allWallsMap.put(String.valueOf(walls.getId()), wallsMap);
			}
		}
		projectData.put("walls", allWallsMap );
		
		

		return projectData;

	}

	/*// @PostConstruct
	void getProjectData() {

		try {

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {
					CompletableFuture<JSONObject> projectInfoCompletableFuture;
					try {
						projectInfoCompletableFuture = quuppaApiService.getProjectInfo();

						CompletableFuture.allOf(projectInfoCompletableFuture).join();

						JSONObject projectsInfoJsonObject = null;
						if (projectInfoCompletableFuture.isDone()) {
							projectsInfoJsonObject = projectInfoCompletableFuture.get();

							if (projectsInfoJsonObject != null) {

								String status = projectsInfoJsonObject.optString("status");

								if (!Utils.isEmptyString(status) && status.equals("success")) {

									HashMap<String, Object> data = new HashMap<>();

									JSONObject projectInfoJsonObject = projectsInfoJsonObject
											.optJSONObject("backgroundImages");

									JSONArray locatorsArray = projectsInfoJsonObject.optJSONArray("locatorsArray");
									if (projectInfoJsonObject != null) {
										double widthMeter = projectInfoJsonObject.getDouble("widthMeter");
										heightMeter = projectInfoJsonObject.getDouble("heightMeter");

										metersPerPixelX = projectInfoJsonObject.getDouble("metersPerPixelX");
										metersPerPixelY = projectInfoJsonObject.getDouble("metersPerPixelY");

										double origoX = projectInfoJsonObject.getDouble("origoX");
										double origoY = projectInfoJsonObject.getDouble("origoY");
										int rotation = projectInfoJsonObject.getInt("rotation");

										xMeter = projectInfoJsonObject.getDouble("xMeter");
										yMeter = projectInfoJsonObject.getDouble("yMeter");

										data.put("widthMeter", widthMeter);
										data.put("heightMeter", heightMeter);
										data.put("metersPerPixelX", metersPerPixelX);
										data.put("metersPerPixelY", metersPerPixelY);
										data.put("origoX", origoX);
										data.put("origoY", origoY);
										data.put("rotation", rotation);
										data.put("image", projectInfoJsonObject.optString("base64"));
									}

									List<HashMap<String, Object>> locatorList = new ArrayList<>();
									if (locatorsArray != null && locatorsArray.length() > 0) {

										DecimalFormat df = new DecimalFormat("#.##");

										for (int i = 0; i < locatorsArray.length(); i++) {
											JSONObject locatorObject = locatorsArray.getJSONObject(i);
											HashMap<String, Object> detailsMap = new HashMap<>();
											detailsMap.put("type", locatorObject.optString("locatorType"));
											detailsMap.put("id", locatorObject.optString("id"));
											detailsMap.put("name", locatorObject.optString("name"));

											JSONArray locationArray = locatorObject.optJSONArray("location");

											if (locationArray != null) {

												double[] arr = Utils.getPoint(locationArray.getDouble(0),
														locationArray.getDouble(1));

												detailsMap.put("x", Double.valueOf(df.format(arr[0])));
												detailsMap.put("y", Double.valueOf(df.format(arr[1])));
												detailsMap.put("z",
														Double.valueOf(df.format(locationArray.getDouble(2))));

											}
											locatorList.add(detailsMap);
										}

									}
									data.put("locatorInfo", locatorList);

									TimeZone zone = TimeZone.getDefault();
									String timeZone = zone.getID();
									if (!Utils.isEmptyString(timeZone))
										data.put("timeZone", timeZone);

									addLogs("Event", "Genie", "Get Project Info-EntappiaGenieApp", "Success");

									//alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_QPE);

									CompletableFuture<JSONObject> sendProjectInfoCompletableFuture = entappiaApiService
											.projectInfoDetails(data);
									if (sendProjectInfoCompletableFuture != null) {
										JSONObject jsonObject = sendProjectInfoCompletableFuture.get();
										if (jsonObject != null) {
											String status2 = jsonObject.optString("status");
											if (status2.equalsIgnoreCase("success")) {
												//ToDo:Shiva
												send
												sendTagData();
												//getTagData();
												//getZoneData();
												//getConfigurationData();
												addLogs("Event", "Genie", "Send Project Info-EntappiaGenieApp",
														"Success");
											} else {
												String message = jsonObject.optString("message");
												addLogs("Error", "Genie", "Send Project Info-EntappiaGenieApp",
														message);
											}
										}
									}

								} else {

									String message = projectsInfoJsonObject.optString("message");
									//alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_QPE, message);

									addLogs("Error", "Genie", "Get Project Info-EntappiaGenieApp", message);
								}

							}
						}

					} catch (InterruptedException e) {
						addLogs("Error", "Genie", "Get Project Info-EntappiaGenieApp", e.getMessage());
						e.printStackTrace();
					} catch (ExecutionException e) {
						addLogs("Error", "Genie", "Get Project Info-EntappiaGenieApp", e.getMessage());
						e.printStackTrace();
					} catch (JSONException e) {
						addLogs("Error", "Genie", "Get Project Info-EntappiaGenieApp", e.getMessage());
						e.printStackTrace();
					}
				}
			});
			executorService.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	// @PostConstruct
	//ToDo:Shiva
	void sendTagData() {

		preference.setLongValue("tagLastModifiedTime", 0);
		tagBackgroundTasks.sendTags();
	}

	void stopScheduleTasks() {
		if (AppConstants.print_log)
			System.out.println("stopScheduleTasks");

		isServerConnected = false;

		if (sendingTagPostionTask != null)
			sendingTagPostionTask.stopSchedule();

		if (helloApiTask != null)
			helloApiTask.stopSchedule();

		if (locatorStatusTask != null)
			locatorStatusTask.stopSchedule();

	}

	void startTagScheduler() {
		if (tagSchedulerTask != null)
			tagSchedulerTask.createSchedule();
	}

	void startScheduleTasks() {

		//isServerConnected = true;

		/*
		 * if (sendingTagPostionTask != null) sendingTagPostionTask.createSchedule();
		 */

		if (helloApiTask != null)
			helloApiTask.createSchedule();

		if (locatorStatusTask != null)
			locatorStatusTask.createSchedule();

		if (sendDeviceDetailsTask != null)
			sendDeviceDetailsTask.sendDeviceDetails();

	}

	// @PostConstruct
	void getTagData1() {

		try {
			ExecutorService executorService = Executors.newFixedThreadPool(10);
			executorService.execute(new Runnable() {

				public void run() {

					try {

						CompletableFuture<JSONObject> tagsCompletableFuture = null;//entappiaApiService.getTagsDetails();
						CompletableFuture.allOf(tagsCompletableFuture).join();
						if (tagsCompletableFuture.isDone()) {
							JSONObject tagsJson;
							try {
								tagsJson = tagsCompletableFuture.get();
								if (tagsJson != null) {
									String status = tagsJson.optString("status");

									if (!Utils.isEmptyString(status)) {
										if (status.equals("success")) {

											//ToDo:Shiva
											/*JSONArray tagArray = tagsJson.optJSONArray("tagArray");
											if (tagArray != null) {

												for (int i = 0; i < tagArray.length(); i++) {

													JSONObject jsonObject = tagArray.getJSONObject(i);

													Tags tags = tagRepository
															.findByTagId(jsonObject.getString("tagId"));
													if (tags == null) {
														tags = new Tags();
													}

													tags.setTagId(jsonObject.getString("tagId"));
													tags.setMacId(jsonObject.getString("macId"));
													tags.setStatus(jsonObject.getString("status"));
													tags.setAssignmentNo(jsonObject.getString("assignmentNo"));
													tags.setAssignTime(jsonObject.getLong("assignTime"));
													tags.setToTime(jsonObject.getLong("toTime"));
													tags.setModifiedTime(jsonObject.optLong("modifiedTime", 0));
													tags.setCreatedTime(jsonObject.optLong("createdTime", 0));

													tagRepository.save(tags);

													if (preference.getLongValue("tagLastModifiedTime") < jsonObject
															.optLong("modifiedTime", 0))
														preference.setLongValue("tagLastModifiedTime",
																jsonObject.optLong("modifiedTime", 0));
												}
											}*/
											addLogs("Event", "Genie", "Get QuuppaTags-EntappiaGenieApp", "Success");
											//alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA);

										} else {
											String message = tagsJson.optString("message");

											//alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA,
											//message);
											addLogs("Error", "Genie", "Get QuuppaTags-EntappiaGenieApp", message);
										}
									}
								}
							} catch (ExecutionException e) {
								addLogs("Error", "Genie", "Get QuuppaTags-EntappiaGenieApp", e.getMessage());
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					} catch (InterruptedException e) {
						addLogs("Error", "Genie", "Get QuuppaTags-EntappiaGenieApp", e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e1) {
						addLogs("Error", "Genie", "Get QuuppaTags-EntappiaGenieApp", e1.getMessage());
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			executorService.shutdown();

		} catch (Exception e) {
			addLogs("Error", "Genie", "Get QuuppaTags-EntappiaGenieApp", e.getMessage());
			e.printStackTrace();
		}

	}

	void getProfileConfiguration() {
		// TODO Auto-generated method stub

		try {
			CompletableFuture<JSONObject> configCompletableFuture = entappiaApiService.getProfileConfiguration();
			if (configCompletableFuture != null) {
				CompletableFuture.allOf(configCompletableFuture).join();
				if (configCompletableFuture.isDone()) {
					try {
						JSONObject configJSONObject = configCompletableFuture.get();
						if (configJSONObject != null) {
							JSONObject jsonObject = configJSONObject.optJSONObject("response");
							if (jsonObject != null && jsonObject.length() > 0) {

								JSONArray profileConfigJsonArray = jsonObject.optJSONArray("configData");
								if (profileConfigJsonArray != null && profileConfigJsonArray.length() > 0) {
								String configDetails = profileConfigJsonArray.toString();
								if (configDetails != null && !configDetails.equals("null")) {
									applicationConfigurationSettings.setConfigDetails(configDetails); 
								}
							}
						}
						}
					}catch (ExecutionException e) {
						addLogs("Error", "Genie", "Get Configuration-EntappiaGenieApp", e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (Exception e) {
						addLogs("Error", "Genie", "Get Configuration-EntappiaGenieApp", e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {

						startScheduleTasks();
					}
				}
			}

		} catch (InterruptedException e) {
			addLogs("Error", "Genie", "Get Configuration-EntappiaGenieApp", e.getMessage());
			e.printStackTrace();
		} finally {

		}

	}

	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);
	}

	public HashMap<String, Tags> getDBTags() {

		HashMap<String, Tags> tagsMap = new HashMap<>();
		//ToDo:Shiva
		//tagRepository.findAll().forEach(e -> tagsMap.put("" + e.getMacId(), e));
		return tagsMap;
	}

}
// @PostConstruct
/*void getZoneData() {

	try {
		CompletableFuture<JSONObject> mistCompletableFuture = entappiaApiService.getZoneDetails();
		if (mistCompletableFuture != null) {
			CompletableFuture.allOf(mistCompletableFuture).join();
			if (mistCompletableFuture.isDone()) {
				try {
					JSONObject zoneJSONObject = mistCompletableFuture.get();
					if (zoneJSONObject != null) {

						String status = zoneJSONObject.optString("status");

						if (!Utils.isEmptyString(status)) {
							if (status.equals("success")) {
								ArrayList<Zones> zoneList = (ArrayList<Zones>) Utils.getObjectList(
										zoneJSONObject.getJSONArray("response").toString(), Zones.class);

								if (zoneList != null && zoneList.size() > 0) {
									DecimalFormat df = new DecimalFormat("#.##");
									for (Zones zones : zoneList) {
										if (zones != null) {

											String polygonData = zones.getPolygonData();
											String[] polygonDataArray = polygonData.split(Pattern.quote("|"));

											ArrayList<String> pointList = new ArrayList<>();

											for (int j = 0; j < polygonDataArray.length; j++) {
												String coordinate = polygonDataArray[j];
												String[] coordinates = coordinate.split(",");

												double x1 = Double.valueOf(coordinates[0]);
												double y1 = Double.valueOf(coordinates[1]);

												double[] arr = Utils.getQuuppaPoint(x1, y1);

												if (arr != null) {
													pointList.add(df.format(arr[0]) + "," + df.format(arr[1]));

												}
											}

											Zones zone = zoneRepository.findByZoneId(zones.getZoneId());
											if (zone == null) {
												zones.setPolygonData(String.join("|", pointList));
												zoneRepository.save(zones);
											} else {
												zones.setPolygonData(String.join("|", pointList));
												zones.setId(zone.getId());
												zoneRepository.save(zones);
											}
											if (preference.getLongValue("zoneLastModifiedTime") < zones
													.getModifiedTime())
												preference.setLongValue("zoneLastModifiedTime",
														zones.getModifiedTime());
										}
									}
								}
								addLogs("Event", "Genie", "Get QuuppaZone-EntappiaGenieApp", "Success");
								alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA);

							} else if (status.equals("success")) {
								String message = zoneJSONObject.optString("message");
								alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_ENTAPPIA, message);
								addLogs("Error", "Genie", "Get QuuppaZone-EntappiaGenieApp", message);
							}
						}
					}

				} catch (ExecutionException e) {
					addLogs("Error", "Genie", "Get QuuppaZone-EntappiaGenieApp", e.getMessage());
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	} catch (InterruptedException e) {
		addLogs("Error", "Genie", "Get QuuppaZone-EntappiaGenieApp", e.getMessage());
		e.printStackTrace();
	}

}*/
/*public boolean checkServerConnection() {

boolean hasServerConnected = false;
try {
	CompletableFuture<Boolean> sessionCompletableFuture = entappiaApiService
			.checkServerConnection(preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME));
	CompletableFuture.allOf(sessionCompletableFuture).join();

	if (sessionCompletableFuture.isDone()) {
		hasServerConnected = sessionCompletableFuture.get();
	}

} catch (InterruptedException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (ExecutionException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return hasServerConnected;
}*/

/*
 * public void stopPrimaryServerChecking() { if (AppConstants.print_log)
 * System.out.println("stopPrimaryServerChecking");
 * checkPrimaryServerTask.stopSchedule(); isPrimaryServer = true;
 * primaryServerErrorCount = 0;
 * 
 * addLogs("Event", "Genie", "Server Connection Checking",
 * "Server switched primary server(" +
 * preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME) + ").");
 * 
 * createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
 * 
 * }
 * 
 * public void stopServerChecking() { if (AppConstants.print_log)
 * System.out.println("stopPrimaryServerChecking");
 * checkServerTask.stopSchedule();
 * 
 * addLogs("Event", "Genie", "Server Connection Checking",
 * "Server switched to seconday server(" +
 * preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME) + ").");
 * 
 * createEntappiaSession(preference.getStringValue(AppConstants.LICENSE_KEY));
 * 
 * }
 * 
 * public void startPrimaryServerChecking() {
 * 
 * if (!isPrimaryServer) { if (AppConstants.print_log)
 * System.out.println("startPrimaryServerChecking");
 * 
 * checkPrimaryServerTask.createSchedule(); } }
 */
//delete tables when org name change
/*public void checkOrganization(String organization) {

		profileConfigurationRepository.findAll().forEach(e -> {

			String org = e.getOrganization();

			if (!Utils.isEmptyString(org) && !org.equals(organization)) {
				//alertsRepository.truncateAlerts();
				profileConfigurationRepository.truncateMyTable();
				genieLogsRepository.truncateMyTable();
				//logsCtProfileRepository.truncateMyTable();
				//tagRepository.truncateMyTable();
				//zoneRepository.truncateMyTable();
				//zoneVisitRepository.truncateMyTable();

				firmwareSettingsRepository.findAll().forEach(e1 -> {

					FirmwareSettings settings = e1;
					if (settings != null) {
						settings.setLogNo(0);
						firmwareSettingsRepository.save(settings);
					}
				});
			}
		});

	}*/
