package com.entappia.ei40genie.models;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.dbmodels.ProfileConfiguration;
import com.entappia.ei40genie.repository.ProfileConfigurationRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component
public class ApplicationConfigurationSettings {

	@Autowired
	private ProfileConfigurationRepository profileConfigurationRepository;

	Preference preference = new Preference();

	public void setConfigDetails(String configDetails, String organization) {
		ProfileConfiguration configuration = new Gson().fromJson(configDetails, ProfileConfiguration.class);
		configuration.setOrganization(organization);
		profileConfigurationRepository.save(configuration);

		HashMap<String, Integer> configMap = new HashMap<>();

		List<ConfigData> configDataList = configuration.getConfigData();
		if (configDataList != null && configDataList.size() > 0) {
			for (int i = 0; i < configDataList.size(); i++) {

				ConfigData configData = configDataList.get(i);

				if (configData != null) {

					if (configData.getName().equals(AppConstants.CONFIG_PSN))
						preference.setStringValue(AppConstants.PRIMARY_SERVER_NAME,
								String.valueOf(configData.getDefaultval()));
					else if (configData.getName().equals(AppConstants.CONFIG_SSN))
						preference.setStringValue(AppConstants.SECONDARY_SERVER_NAME,
								String.valueOf(configData.getDefaultval()));
					else if (configData.getName().equals(AppConstants.CONFIG_TTA))
						preference.setIntValue(AppConstants.CONFIG_PREF_TTA,
								Integer.parseInt(configData.getDefaultval()));
					else if (configData.getName().equals(AppConstants.CONFIG_LTA))
						preference.setIntValue(AppConstants.CONFIG_PREF_LTA,
								Integer.parseInt(configData.getDefaultval()));

					else if (configData.getName().equals(AppConstants.CONFIG_HA))
						preference.setIntValue(AppConstants.CONFIG_PREF_HA,
								Integer.parseInt(configData.getDefaultval()));
					else if (configData.getName().equals(AppConstants.CONFIG_LTT))
						preference.setIntValue(AppConstants.CONFIG_PREF_LTT,
								Integer.parseInt(configData.getDefaultval()));
					else if (configData.getName().equals(AppConstants.CONFIG_LS))
						preference.setIntValue(AppConstants.CONFIG_PREF_LS,
								Integer.parseInt(configData.getDefaultval()));
					else if (configData.getName().equals(AppConstants.CONFIG_DUR)) {
						preference.setIntValue(AppConstants.CONFIG_PREF_DUR,
								Integer.parseInt(configData.getDefaultval()));

						configMap.put(AppConstants.CONFIG_PREF_DUR, Integer.parseInt(configData.getDefaultval()));

					} else if (configData.getName().equals(AppConstants.CONFIG_ARC)) {
						preference.setIntValue(AppConstants.CONFIG_PREF_ARC,
								Integer.parseInt(configData.getDefaultval()));

						configMap.put(AppConstants.CONFIG_PREF_ARC, Integer.parseInt(configData.getDefaultval()));
					} else if (configData.getName().equals(AppConstants.CONFIG_ARI)) {
						preference.setIntValue(AppConstants.CONFIG_PREF_ARI,
								Integer.parseInt(configData.getDefaultval()));
						configMap.put(AppConstants.CONFIG_PREF_ARI, Integer.parseInt(configData.getDefaultval()));
					} else if (configData.getName().equals(AppConstants.CONFIG_STF)) {
						preference.setIntValue(AppConstants.CONFIG_PREF_STF,
								Integer.parseInt(configData.getDefaultval()));
						configMap.put(AppConstants.CONFIG_PREF_STF, Integer.parseInt(configData.getDefaultval()));
					} else if (configData.getName().equals(AppConstants.CONFIG_AST)) {
						preference.setIntValue(AppConstants.CONFIG_PREF_AST,
								Integer.parseInt(configData.getDefaultval()));
						configMap.put(AppConstants.CONFIG_PREF_AST, Integer.parseInt(configData.getDefaultval()));
					} else if (configData.getName().equals(AppConstants.CONFIG_VIS)) {
						preference.setIntValue(AppConstants.CONFIG_PREF_VIS,
								Integer.parseInt(configData.getDefaultval()));
						configMap.put(AppConstants.CONFIG_PREF_VIS, Integer.parseInt(configData.getDefaultval()));
					} else if (configData.getName().equals(AppConstants.CONFIG_MAT)) {
						preference.setIntValue(AppConstants.CONFIG_PREF_MAT,
								Integer.parseInt(configData.getDefaultval()));
						configMap.put(AppConstants.CONFIG_PREF_MAT, Integer.parseInt(configData.getDefaultval()));
					}

					if (preference.getLongValue(AppConstants.CONFIG_LMT) < configData.getModifiedTime())
						preference.setLongValue(AppConstants.CONFIG_LMT, configData.getModifiedTime());
				}
			}
		}
	}

	public void setConfigDetails(String configDetails) {

		List<ConfigData> configListnew = new Gson().fromJson(configDetails, new TypeToken<List<ConfigData>>() {
		}.getType());

		ProfileConfiguration configurationval = profileConfigurationRepository
				.findByOrganization(preference.getStringValue(AppConstants.ORGANIZATION_NAME));
		if (configurationval == null) {
			configurationval = new ProfileConfiguration();
			configurationval.setOrganization(preference.getStringValue(AppConstants.ORGANIZATION_NAME));
		}

		if (configurationval != null && configListnew!=null) {

			configurationval.setConfigData(configListnew);
			profileConfigurationRepository.save(configurationval);

			List<ConfigData> configDataList = configurationval.getConfigData();

			HashMap<String, Integer> configMap = new HashMap<>();

			if (configDataList != null && configDataList.size() > 0) {

				for (int i = 0; i < configDataList.size(); i++) {

					ConfigData configData = configDataList.get(i);

					if (configData != null) {
						if (preference.getLongValue(AppConstants.CONFIG_LMT) < configData.getModifiedTime())
							preference.setLongValue(AppConstants.CONFIG_LMT, configData.getModifiedTime());

						if (configData.getName().equals(AppConstants.CONFIG_PSN))
							preference.setStringValue(AppConstants.PRIMARY_SERVER_NAME,
									String.valueOf(configData.getDefaultval()));
						else if (configData.getName().equals(AppConstants.CONFIG_SSN))
							preference.setStringValue(AppConstants.SECONDARY_SERVER_NAME,
									String.valueOf(configData.getDefaultval()));
						else if (configData.getName().equals(AppConstants.CONFIG_TTA))
							preference.setIntValue(AppConstants.CONFIG_PREF_TTA,
									Integer.parseInt(configData.getDefaultval()));
						else if (configData.getName().equals(AppConstants.CONFIG_LTA))
							preference.setIntValue(AppConstants.CONFIG_PREF_LTA,
									Integer.parseInt(configData.getDefaultval()));
						else if (configData.getName().equals(AppConstants.CONFIG_HA)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_HA,
									Integer.parseInt(configData.getDefaultval()));

						} else if (configData.getName().equals(AppConstants.CONFIG_LTT)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_LTT,
									Integer.parseInt(configData.getDefaultval()));

						} else if (configData.getName().equals(AppConstants.CONFIG_ARC)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_ARC,
									Integer.parseInt(configData.getDefaultval()));
							configMap.put(AppConstants.CONFIG_PREF_ARC, Integer.parseInt(configData.getDefaultval()));
						}

						else if (configData.getName().equals(AppConstants.CONFIG_LS)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_LS,
									Integer.parseInt(configData.getDefaultval()));
						} else if (configData.getName().equals(AppConstants.CONFIG_DUR)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_DUR,
									Integer.parseInt(configData.getDefaultval()));

							configMap.put(AppConstants.CONFIG_PREF_DUR, Integer.parseInt(configData.getDefaultval()));
						} else if (configData.getName().equals(AppConstants.CONFIG_ARI)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_ARI,
									Integer.parseInt(configData.getDefaultval()));
							configMap.put(AppConstants.CONFIG_PREF_ARI, Integer.parseInt(configData.getDefaultval()));
						} else if (configData.getName().equals(AppConstants.CONFIG_STF)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_STF,
									Integer.parseInt(configData.getDefaultval()));
							configMap.put(AppConstants.CONFIG_PREF_STF, Integer.parseInt(configData.getDefaultval()));
						} else if (configData.getName().equals(AppConstants.CONFIG_AST)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_AST,
									Integer.parseInt(configData.getDefaultval()));
							configMap.put(AppConstants.CONFIG_PREF_AST, Integer.parseInt(configData.getDefaultval()));
						} else if (configData.getName().equals(AppConstants.CONFIG_VIS)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_VIS,
									Integer.parseInt(configData.getDefaultval()));
							configMap.put(AppConstants.CONFIG_PREF_VIS, Integer.parseInt(configData.getDefaultval()));
						} else if (configData.getName().equals(AppConstants.CONFIG_MAT)) {
							preference.setIntValue(AppConstants.CONFIG_PREF_MAT,
									Integer.parseInt(configData.getDefaultval()));
							configMap.put(AppConstants.CONFIG_PREF_MAT, Integer.parseInt(configData.getDefaultval()));
						}
					}

				}
			}

		}
	}

}
