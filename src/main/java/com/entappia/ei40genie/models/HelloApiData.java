package com.entappia.ei40genie.models;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class HelloApiData{
	
	private List<HashMap<String, Object>> tagDetails = new ArrayList<>();; 
	private HashMap<String, Object> tagColorDetails = new HashMap<>();
	private HashMap<String, List<String>> removeZoneDetails = new HashMap<>();
	private HashMap<String, Object> addZoneDetails = new HashMap<>();
	private HashMap<String, List<String>> removeMapDetails = new HashMap<>();
	private HashMap<String, Object> mapDetails = new HashMap<>();
	private HashMap<String, Object> applicationVersion = new HashMap<>();
	
	private List<HashMap<String, Object>> doors = new ArrayList<>();
	private List<HashMap<String, Object>> partitions = new ArrayList<>();
	private List<HashMap<String, Object>> pathways = new ArrayList<>();
	private List<HashMap<String, Object>> walls = new ArrayList<>();
	private List<HashMap<String, Object>> outlines = new ArrayList<>();
	
	
	
	public void clear() {
		tagColorDetails.clear();
		tagDetails.clear();
		removeZoneDetails.clear();
		addZoneDetails.clear();
		removeMapDetails.clear();
		mapDetails.clear();
		applicationVersion.clear();
		doors.clear();
		partitions.clear();
		pathways.clear();
		walls.clear();
		outlines.clear();
	}
	
	public HashMap<String, Object> getTagColorDetails() {
		if(tagColorDetails!= null && tagColorDetails.size() > 0)
		return tagColorDetails;
		return null;
	}
	
	public void setTagColorDetails(String tagName, String color) {
		tagColorDetails.put(tagName, color);
	}
	
	public List<HashMap<String, Object>> getTagDetails() {
		if(tagDetails!= null && tagDetails.size() > 0)
		return tagDetails;
		return null;
	}
	
	public void setTagDetails(HashMap<String, Object> tagDetails) {
		this.tagDetails.add(tagDetails);
	}

	public HashMap<String, List<String>> getRemoveZoneDetails() {
		if(removeZoneDetails!= null && removeZoneDetails.size() > 0)
		return removeZoneDetails;
		return null;
	}

	public void setRemoveZoneDetails(HashMap<String, List<String>> removeZoneDetails) {
		this.removeZoneDetails = removeZoneDetails;
	}

	public HashMap<String, Object> getAddZoneDetails() {
		if(addZoneDetails!= null && addZoneDetails.size() > 0)
		return addZoneDetails;
		return null;
	}

	@SuppressWarnings("unchecked")
	public void setAddZoneDetails( Object addZoneDetails) {
		this.addZoneDetails = (HashMap<String, Object>) addZoneDetails;
	}
	
	public HashMap<String, List<String>> getRemoveMapDetails() {
		if(removeMapDetails!= null && removeMapDetails.size() > 0)
		return removeMapDetails;
		return null;
	}

	public void setRemoveMapDetails(HashMap<String, List<String>> removeMapDetails) {
		this.removeMapDetails = removeMapDetails;
	}
	public HashMap<String, Object> getApplicationVersion() {
		if(applicationVersion!= null && applicationVersion.size() > 0)
		return applicationVersion;
		return null;
	}
	
	public void setApplicationVersion(String tagName, String color) {
		applicationVersion.put(tagName, color);
	}
	public HashMap<String, Object> getMapDetails() {
		if(mapDetails!= null && mapDetails.size() > 0)
			return mapDetails;
		return null;
	}
	
	public void setMapDetails(String tagName, Object color) {
		mapDetails.put(tagName, color);
	}

	public List<HashMap<String, Object>> getDoors() {
		if(doors!= null && doors.size() > 0)
			return doors;
		return null;
	}

	public void setDoors(List<HashMap<String, Object>> doors) {
		this.doors = doors;
	}

	public List<HashMap<String, Object>> getPartitions() {
		if(partitions!= null && partitions.size() > 0)
			return partitions;
		return null;
	}

	public void setPartitions(List<HashMap<String, Object>> partitions) {
		this.partitions = partitions;
	}

	public List<HashMap<String, Object>> getPathways() {
		if(pathways!= null && pathways.size() > 0)
			return pathways;
		return null;
	}

	public void setPathways(List<HashMap<String, Object>> pathways) {
		this.pathways = pathways;
	}

	public List<HashMap<String, Object>> getWalls() {
		if(walls!= null && walls.size() > 0)
			return walls;
		return null;
	}

	public void setWalls(List<HashMap<String, Object>> walls) {
		this.walls = walls;
	}

	public List<HashMap<String, Object>> getOutlines() {
		if(outlines!= null && outlines.size() > 0)
			return outlines;
		return null;
	}

	public void setOutlines(List<HashMap<String, Object>> outlines) {
		this.outlines = outlines;
	}

	
	
}
  