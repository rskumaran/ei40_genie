package com.entappia.ei40genie.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei40genie.dbmodels.ProfileConfiguration;

@Repository
public interface ProfileConfigurationRepository extends CrudRepository<ProfileConfiguration, String> {
	ProfileConfiguration findByOrganization(String organization);
	
	@Modifying
	@Transactional
	@Query(value = "truncate table Profile_Configuration", nativeQuery = true)
	void truncateMyTable();
  }
 