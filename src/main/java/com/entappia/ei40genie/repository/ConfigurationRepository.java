package com.entappia.ei40genie.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei40genie.dbmodels.Configuration;
 
@Repository
public interface ConfigurationRepository extends CrudRepository<Configuration, Integer> {
	Configuration findByOrgName(String orgName);
	
	List<Configuration> findAll();

}
