package com.entappia.ei40genie.repository;
 
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei40genie.dbmodels.CampusDetails;

@Repository
public interface CampusDetailsRepository extends CrudRepository<CampusDetails, Integer>{

	CampusDetails findByCampusId(long campusId);
	List<CampusDetails> findAll();
}
