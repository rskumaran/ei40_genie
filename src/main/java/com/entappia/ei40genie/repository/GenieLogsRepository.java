package com.entappia.ei40genie.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei40genie.dbmodels.GenieLogs;
@Repository
public interface GenieLogsRepository extends CrudRepository<GenieLogs, Integer> {
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM genie_logs WHERE date < :date", nativeQuery = true)  
	int deleteByDate(@Param("date") Date date);
	
	/*@Query(value = "select * from genie_logs where log_id > :log_id", nativeQuery = true)  
	List<GenieLogs> findAllWithNewLogs(@Param("log_id") int log_id);*/
	
	@Modifying
	@Transactional
	@Query(value = "truncate table genie_logs", nativeQuery = true)
	void truncateMyTable();
	
}
