package com.entappia.ei40genie;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

import com.entappia.ei40genie.constants.AppConstants;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class Utils {

	static boolean syslog = true;

	public static long getUtcTime(Date date1) {
		if (date1 != null) {
			OffsetDateTime now = date1.toInstant().atOffset(ZoneOffset.UTC);
			return now.toInstant().toEpochMilli(); 
		} else
			return 0;
	}

 	public static boolean isEmptyString(String value) {
		return value == null || value.trim().isEmpty() || value.equals("null");
	}

	public static String getDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date);

	}

	public static String getFormatedDate(Date date) {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
		return dateTimeFormat.format(date);

	}

	public static String getFormatedDate1(Date date) {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
		return dateTimeFormat.format(date);

	}
	
	public static String getFormatedDate2(Date date) {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss.SSS");
		return dateTimeFormat.format(date);

	}

	public static Date getDate(String date) {
		try {
			if (AppConstants.print_log)
				System.out.println("Date:" + date);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			return dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static String getDateTime1(Date date) {
		SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return DATE_TIME_FORMAT.format(date);
	}

	public static Date getDateTime(String date) {
		try {
			SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			return DATE_TIME_FORMAT.parse(date);
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}

	}

	public static String getTime(Date date) {
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		return timeFormat.format(date);
	}

	public static int getDayMintus(Date date) {
		String sTime = Utils.getTime(date);
		String hours = sTime.split(":")[0];
		String mins = sTime.split(":")[1];

		int min = Integer.valueOf(mins);

		int dayMintus = 0;
		if (Integer.valueOf(hours) == 0) {
			dayMintus = (min - 1);
		} else {
			dayMintus = ((Integer.valueOf(hours) * 60) + min - 1);
		}

		return dayMintus;
	}

	public static Date stringToDate(String date) {

		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static <T> List<T> getObjectList(String jsonString, Class<T> cls) {
		List<T> list = new ArrayList<T>();
		try {
			Gson gson = new Gson();
			JsonArray arry = new JsonParser().parse(jsonString).getAsJsonArray();
			for (JsonElement jsonElement : arry) {
				list.add(gson.fromJson(jsonElement, cls));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static double[] getQuuppaPoint(double x, double y) {

		y = EntappiaGenieApplication.heightMeter - y;

		x = EntappiaGenieApplication.xMeter + x;
		y = EntappiaGenieApplication.yMeter + y;

		return new double[] { x, y };

	}

	public static double[] getPoint(double x, double y) {

		double originX = EntappiaGenieApplication.xMeter;
		double originY = EntappiaGenieApplication.heightMeter + EntappiaGenieApplication.yMeter;

		y = y - originY;
		x = x - originX;

		if (y < 0)
			y = y * -1;

		if (x < 0)
			x = x * -1;

		return new double[] { x, y };

	}
	
	public static String getServerPath() {
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		String os = System.getProperty("os.name").toLowerCase();
		String dir = "";
		if (os.contains("windows")) {
			dir = bundle.getString("serverpath_windows");
		} else if (os.contains("mac os")) {
			dir = bundle.getString("serverpath_ios");
		} else
			dir = bundle.getString("serverpath_linux");

		return dir;
	}
	
	public static String getEI40ServerPath() {
		  
		return getServerPath()+"/EI40";
	}


	public static String getDownloadServerPath() {
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		String os = System.getProperty("os.name").toLowerCase();
		String dir = "";
		if (os.contains("windows")) {
			dir = bundle.getString("download_serverpath_windows");
		} else if (os.contains("mac os")) {
			dir = bundle.getString("download_serverpath_ios");
		} else
			dir = bundle.getString("download_serverpath_linux");

		return dir;
	}

}
