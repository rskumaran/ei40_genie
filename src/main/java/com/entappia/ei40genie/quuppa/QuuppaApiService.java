package com.entappia.ei40genie.quuppa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.RestErrorHandler;
import com.entappia.ei40genie.constants.AppConstants;


@Service
public class QuuppaApiService {

	@SuppressWarnings("unused")
	@Async
	public CompletableFuture<JSONObject> getTagDetails1() throws InterruptedException {

		String zoneUrl = getQuuppaURL(AppConstants.GET_TAG_URL);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		JSONObject json = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(zoneUrl, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String zones = response.getBody();
				try {
					JSONObject tagsObject = new JSONObject(zones);
					if (tagsObject != null) {
						json.put("response", tagsObject);
						json.put("status", "success");

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				json.put("status", "error");
				json.put("message", "Status Code:" + response.getStatusCodeValue() + ", Connection Failed");

			}
		} catch (ResourceAccessException e) {
			json.put("status", "error");
			json.put("message", e.getMessage());
		}

		return CompletableFuture.completedFuture(json);
	}

	@Async
	public CompletableFuture<JSONObject> getProjectInfo() throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = getQuuppaURL(AppConstants.GET_PROJECT_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());
		
		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		JSONObject projectInfoJsonObject = new JSONObject();
		 
		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String projectInfo = response.getBody();
				JSONObject json = new JSONObject();

				try {
					json = new JSONObject(projectInfo);

					if (json != null) {

						int code = json.getInt("code");
						if (code == 0) {

							JSONArray jsonArray = json.getJSONArray("coordinateSystems");
							if (jsonArray != null && jsonArray.length() > 0) {

								JSONObject json1 = jsonArray.getJSONObject(0);
								if (json1 != null) {

									JSONArray projectInfoArray = json1.getJSONArray("backgroundImages");
									if (projectInfoArray != null && projectInfoArray.length() > 0) {
										JSONObject projectJsonObject = projectInfoArray.getJSONObject(0);
										projectInfoJsonObject.put("backgroundImages", projectJsonObject);
									}

									JSONArray locatorsArray = json1.getJSONArray("locators");
									if (locatorsArray != null && locatorsArray.length() > 0) {
										projectInfoJsonObject.put("locatorsArray", locatorsArray);

									}

									projectInfoJsonObject.put("status", "success");
								}
							}
						} else {
							String message = json.getString("message");
							projectInfoJsonObject.put("status", "error");
							projectInfoJsonObject.put("message", message);
						}
					}

				} catch (JSONException e) {
					projectInfoJsonObject.put("status", "error");
					projectInfoJsonObject.put("message", e.getMessage());
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			} else {

				projectInfoJsonObject.put("status", "error");
				projectInfoJsonObject.put("message",
						"Status Code:" + response.getStatusCodeValue() + ", Connection Failed");

			}
		} catch (ResourceAccessException e) {
			projectInfoJsonObject.put("status", "error");
			projectInfoJsonObject.put("message", e.getMessage());
		}
		return CompletableFuture.completedFuture(projectInfoJsonObject);
	}

	@Async
	public CompletableFuture<JSONObject> getQPEInfo() throws InterruptedException {

		String url = getQuuppaURL(AppConstants.GET_QPE_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			String licenseKey = null;

			if (response.getStatusCodeValue() == 200) {

				String projectInfo = response.getBody();
				JSONObject json = new JSONObject();

				try {
					json = new JSONObject(projectInfo);
					if (json != null) {
						int code = json.getInt("code");
						if (code == 0) {
							JSONObject peJsonObject = json.getJSONObject("positioningEngine");
							if (peJsonObject != null) { 
								licenseKey = peJsonObject.optString("licenseKey", "");
								jsonObject.put("licenseKey", licenseKey);
								jsonObject.put("memoryFree", peJsonObject.optLong("memoryFree", 0));
								jsonObject.put("memoryMax", peJsonObject.optLong("memoryMax", 0));
								jsonObject.put("memoryUsed", peJsonObject.optLong("memoryUsed", 0));
								  
								jsonObject.put("memoryAllocated", peJsonObject.optLong("memoryAllocated", 0));
								jsonObject.put("diskFree", peJsonObject.optLong("diskFree", 0));
								jsonObject.put("cpuLoad", peJsonObject.optDouble("cpuLoad", 0));
								
								jsonObject.put("qpeVersion", peJsonObject.optString("qpeVersion", ""));
								jsonObject.put("status", "success");
							}
						} else {
							String message = json.getString("message");
							jsonObject.put("status", "error");
							jsonObject.put("message", message);
						}

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Status Code:" + response.getStatusCodeValue() + ", Connection Failed");

			}
		} catch (ResourceAccessException e) {
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", e.getMessage());
		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	@Async
	public CompletableFuture<JSONObject> getLocatorInfo() throws InterruptedException {

		String url = getQuuppaURL(AppConstants.GET_LOCATOR_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			String licenseKey = null;

			if (response.getStatusCodeValue() == 200) {

				String projectInfo = response.getBody();
				JSONObject json = new JSONObject();

				try {
					json = new JSONObject(projectInfo);
					if (json != null) {
						int code = json.getInt("code");
						if (code == 0) {
							jsonObject.put("status", "success");
							JSONArray locatorsJSONArray = json.getJSONArray("locators");
							if (locatorsJSONArray != null) {

								jsonObject.put("locators", locatorsJSONArray);

							}
						} else {
							String message = json.getString("message");
							jsonObject.put("status", "error");
							jsonObject.put("message", message);
						}

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Status Code:" + response.getStatusCodeValue() + ", Connection Failed");

			}
		} catch (ResourceAccessException e) {
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", e.getMessage());
		}
		return CompletableFuture.completedFuture(jsonObject);
	}


	public String getQuuppaURL(String url)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("application"); 
		return bundle.getString("QUUPPA_HOST_NAME") + url;
	}

}
