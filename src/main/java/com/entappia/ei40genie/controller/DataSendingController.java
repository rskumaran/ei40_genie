package com.entappia.ei40genie.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei40genie.EntappiaGenieApplication;
import com.entappia.ei40genie.Preference;
import com.entappia.ei40genie.Utils;
import com.entappia.ei40genie.constants.AppConstants;
import com.entappia.ei40genie.utils.Logger;


@RestController
public class DataSendingController {


	@Autowired
	Logger logger;
	
	Preference preference = new Preference();

	@CrossOrigin(origins = "*")
	@RequestMapping(path = { "check" }, method = RequestMethod.GET)
	public ResponseEntity<?> checkConnection() throws InterruptedException, ExecutionException, JSONException {

		HashMap<String, Object> secondary = new HashMap<>();
		secondary.put("SECONDARY_SESSION_KEY", preference.getStringValue("secondary_" + AppConstants.SESSION_KEY));
		secondary.put("SECONDARY_SESSION_TIME", preference.getLongValue("secondary_" + AppConstants.SESSION_TIME));
		secondary.put("SECONDARY_SESSION_TIME 1", preference.getLongValue("secondary_" + AppConstants.SESSION_TIME) > 0
				? Utils.getFormatedDate1(new Date(preference.getLongValue("secondary_" + AppConstants.SESSION_TIME)))
				: "-");
		secondary.put("SECONDARY_SERVER_NAME", preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME));

		HashMap<String, Object> primary = new HashMap<>();
		primary.put("PRIMARY_SESSION_KEY", preference.getStringValue("primary_" + AppConstants.SESSION_KEY));
		primary.put("PRIMARY_SESSION_TIME", preference.getLongValue("primary_" + AppConstants.SESSION_TIME));
		primary.put("PRIMARY_SESSION_TIME 1", preference.getLongValue("primary_" + AppConstants.SESSION_TIME) > 0
				? Utils.getFormatedDate1(new Date(preference.getLongValue("primary_" + AppConstants.SESSION_TIME)))
				: "-");

		primary.put("PRIMARY_SERVER_NAME", preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME));

		HashMap<String, Object> returnmap = new HashMap<>();
		returnmap.put("isServerConnected", EntappiaGenieApplication.isServerConnected);
		returnmap.put("isPrimaryServer", EntappiaGenieApplication.isPrimaryServer);
		returnmap.put("CONNECTED_SERVER_NAME",
				EntappiaGenieApplication.isPrimaryServer ? preference.getStringValue(AppConstants.PRIMARY_SERVER_NAME)
						: preference.getStringValue(AppConstants.SECONDARY_SERVER_NAME));
		returnmap.put("primary", primary);
		returnmap.put("secondary", secondary);
		returnmap.put("LOGS", AppConstants.print_log ? "Print logs enabled" : "Print logs disabled");

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}

	@RequestMapping(path = { "printlogs/{isPrint}" }, method = RequestMethod.GET)
	public ResponseEntity<?> changePrintLogs(@PathVariable Optional<Integer> isPrint)
			throws InterruptedException, ExecutionException, JSONException {

		if (isPrint.get() == 0)
			AppConstants.print_log = true;
		else
			AppConstants.print_log = false;

		HashMap<String, Object> returnmap = new HashMap<>();

		returnmap.put("message", AppConstants.print_log ? "Print logs enabled" : "Print logs disabled");
		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}

	public void addLogs(String type, String major, String minor, String message) {
		logger.addLogs(type, major, minor, message);
	}

}
